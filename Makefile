#
#
#
NAME		:= BattleOfTheBulgeSmithsonian
VERSION		:= 1.0-1
VMOD_VERSION	:= 1.2.0
VMOD_TITLE	:= Battle of the Bulge - Smithsonian
DOCFILES	:= $(NAME).tex			\
		   front.tex			\
		   preface.tex			\
		   rules.tex			\
		   materials.tex		
DATAFILES	:= board.tex			\
		   hexes.tex			\
		   oob.tex			\
		   chits.tex			\
		   tables.tex			\
		   probabilities.tex		\
		   detachments.tex		\
		   wargame.speckle.tex
OTHER		:= probabilities.py
STYFILES	:= sbotb.sty			\
		   commonwg.sty
BOARD		:= hexes.pdf
BOARDS		:= hexes.pdf			
SIGNATURE	:= 28
PAPER		:= --a4paper
TARGETSA4	:= $(NAME)A4.pdf 		\
		   $(NAME)A4Booklet.pdf 	\
		   $(NAME)A3Booklet.pdf 	\
		   materialsA4.pdf		\
		   hexes.pdf			\
		   boardA3.pdf			\
		   boardA4.pdf			

TARGETSLETTER	:= $(NAME)Letter.pdf 		\
		   $(NAME)LetterBooklet.pdf 	\
		   materialsLetter.pdf		\
		   boardTabloid.pdf		\
		   boardLetter.pdf

include Variables.mk
include Patterns.mk

all:	a4
a4:	$(TARGETSA4) vmod
letter:	$(TARGETSLETTER)
vmod:	$(NAME).vmod
mine:	a4 cover.pdf box.pdf detachmentsA4.pdf 

.imgs/chits.png:chitsA4.png
	mv $< $@

.imgs/hexes.png:hexes.png
	mv $< $@

.imgs/tables.png:tables.png
	mv $< $@

.imgs/probabilities.png:probabilities.png
	mv $< $@

.imgs/al-oob16.png:oobA4.pdf
	$(PDFTOCAIRO) $(CAIROFLAGS) -png $<
	mv oobA4-1.png	.imgs/al-oob16.png
	mv oobA4-3.png	.imgs/al-oob22.png
	mv oobA4-5.png	.imgs/de-oob16.png
	mv oobA4-7.png	.imgs/de-oob22.png
	rm -f oobA4*.png

.imgs/front.png:front.png
	mv $< $@

update-images:	.imgs/chits.png 	\
		.imgs/hexes.png 	\
		.imgs/al-oob16.png 	\
		.imgs/tables.png	\
		.imgs/front.png		\
		.imgs/probabilities.png

include Rules.mk

# These generate images that are common for all formats 
hexes.pdf:		hexes.tex	wargame.speckle.pdf	$(STYFILES)
export.pdf:		export.tex	probabilities.pdf
wargame.speckle.pdf:	wargame.speckle.tex

$(NAME)A4.pdf:		$(NAME)A4.aux
$(NAME)A4.aux:		$(DOCFILES) 	$(BOARDS) 	$(STYFILES) 	\
			probabilities.pdf
materialsA4.pdf:	materials.tex 	$(BOARDS) 	$(DATAFILES)	\
			probabilities.pdf
boardA4.pdf:		board.tex	$(BOARDS)	$(STYFILES)
boardA3.pdf:		board.tex	$(BOARDS) 	$(STYFILES)
oobA4.pdf:		oob.tex				$(STYFILES)
frontA4.pdf:		front.tex       		$(STYFILES)
chitsA4.pdf:		chits.tex       		$(STYFILES)

$(NAME)Letter.pdf:	$(NAME)Letter.aux
$(NAME)Letter.aux:	$(DOCFILES) 	$(BOARDS)	$(STYFILES) 	\
			probabilities.pdf
materialsLetter.pdf:	materials.tex 	$(BOARDS) 	$(DATAFILES) 	\
			probabilities.pdf
boardLetter.pdf:	board.tex	$(BOARDS)	$(STYFILES)
boardTabloid.pdf:	board.tex	$(BOARDS)	$(STYFILES)
oobLetter.pdf:		oob.tex				$(STYFILES)

$(NAME)LetterBooklet.pdf:PAPER=--letterpaper
$(NAME)LetterBooklet.pdf:SIGNATURE=28

$(NAME).vmod:		TUTORIAL:=Tutorial.vlog

$(NAME)Rules.pdf:	$(NAME)Rules.aux
$(NAME)Rules.aux:	$(DOCFILES) 	$(STYFILES) 	$(BOARDS)

cover.pdf:		cover.tex $(DATAFILES) $(STYFILES)


include Docker.mk

docker-prep::
	mktextfm frunmn
	mktextfm frunbn

docker-artifacts::
	cp $(NAME).vmod $(NAME)-A4-$(VERSION)/


.PRECIOUS:	export.pdf $(NAME)Rules.pdf $(NAME).vtmp

#
# EOF
#

