from wgexport import *
from pprint import pprint

# TODO:
# 
# Nice-to-have
# - Increment German fuel on depot capture
#
# Fixed, but need verification 
# - Problem in T16  with 22 setup   - both factions present, also on OOBs
# 
doTutorial = False # Change this to false for final 

moreHelp = '''
<html>
 <head>
  <title>{title}</title>
  <style>
  </style>
 </head>
 <body>
  <h1>{title}</h1>
  <h2>Content</h2>
  <ul>
    <li><a href="#rules">Rules</a></li>
    <li><a href="#optional">Optional rules and start turn</a></li>
    <li><a href="#turn">Turn and phase tracker</a></li>
    <li><a href="#weather">Weather</a></li>
    <li><a href="#fuel">German fuel reserves</a></li>
    <li><a href="#reinforcements">Reinforcements</a></li>
    <li><a href="#battle">Battle declaration and resolution</a></li>
    <li><a href="#eliminate">Eliminated units</a></li>
    <li><a href="#supply">Supply</a></li>
    <li><a href="#detachements">Regiment detachements</a></li>
    <li><a href="#about">About this game</a></li>
    <li><a href="#changes">Change log</a></li>
    <li><a href="#credits">Credits</a></li>
    <li><a href="#copyright">Copyright and license</a></li>
  </ul>

  <h2><a name="rules"></a>Rules</a>
  <p>
    A PDF of the rules is available through the <b>Help</b> menu.
    Please note that it may take a little while for your PDF viewer
    to launch
  </p>
  <h2><a name="optional"></a>Optional rules and start turn</h2>

  <p> At the start of a new game, you will be presented with a window
   to select which optional rules should be in effect.  You will
   <i>not</i> be able to change these settings later in the game.
  </p>

  <p>In this window you can also select the start turn to start from -
    either the 16th or 22nd of December.</p>

  <p> When the game is progressed to the first phase (<b>Weather</b>
    if enabled), then game pieces that are not in used are removed and
    the optional rules are locked. The units are also moved to their
    starting position according to the choice of start turn.  </p>

  <p> I you play by email, then one of you should select the optional
    rules, save the game <i> before</i> moving to the first real
    phase, and send it off to the other side for validation. Only then
    should you progress to the next phase.  </p>

  <p><b>Note</b>: If you start with the 22nd of December set-up, then
    the turn numbers will still be reported as 1, 2, ..., but the turn
    track will be starting from Dec 22.  This is because VASSAL does
    not allow one to skip turns in an efficient manner.</p>

  <h2><a name="turn"></a>Turn and phase tracker</h2>

  <p> Use the <b>Turn tracker</b> interface in the menubar.  A number
    of automatic actions are implemented in this module, which heavily
    depend on the use of that interface.  </p>

  <p> You can go to the next phase by pressing the <b>+</b> button, or
    by pressing <code>Alt-T</code>.  This is the <i>only</i> possible
    way of moving the turn marker.</p>

  <p><b>Note</b>: The die-roll pop-up windows may steal focus from the
    main window, which means that key-board short-cuts, such as
    <code>Alt-T</code> are not registered.  Simply click the main
    window to bring that back into focus, and keyboard short cuts will
    be registered again.</p>

  <p> Phases that are not in use - because some optional rule was
    disabled, like the <b>air unit</b> phases - are automatically
    skipped.  </p>

  <h2><a name="weather"></a><img src="weather.png" width=24 height=24/>
    &nbsp;Weather</h2>

  <p> If the <b>Weather</b> optional rule is in effect, then the
    weather will automatically be resolved at the start of the
    <b>Weather</b> phase.  </p>

  <h2><a name="fuel"></a><img src="de_fuel.png" width=24 height=24/>&nbsp;German fuel
    reserves</h2>

  <p>German fuel reserves are automatically updated in each activation
   phase.  Should the German faction capture Allied fuel depots, then
   the German faction <i>must</i> make sure to add those fuel points
   to its OOB.  A friendly reminder is displayed in the chat.</p>

  <p>German fuel reserves are <i>not</i> automatically spent on
    activation of German units.  Instead, the German faction must be
    conciencious, and the Allied faction vigilant, and keep score by
    hand in the OOB.  Use the arrows on top to increase and decrease
    the number of available fuel resources.</p>

  <h2><a name="reinforcements"></a>Reinforcements</h2>

  <p>Reinforcements units are <i>not</i> moved on to the map
    automatically.</p>

  <h2><a name="battle"></a>
    <img src="battle-marker-icon.png" width=24 height=24/>
    &nbsp;Battle declaration and resolution</h2>

  <p>Battles <i>must</i> be declared in a factions <b>movement</b>
    phase, including Air <i>attacking</i> CAS sorties.</p>

  <table>
    <tr>
      <td>
        <img src="combat_situation.png">
      </td>
      <td>
        <img src="combat_select.png">
      </td>
      <td>
        <img src="combat_odds.png">
      </td>
    </tr>
    <tr>
      <td>
        Situation
      </td>
      <td>
        Select units
      </td>
      <td>
        Calculate combat differential (<code>Ctrl-X</code>)
      </td>
    </tr>
  </table>

  <p>The module can automatically calculate odds and resolve combats
    via the use of <i>battle markers</i>.  To place a battle marker,
    select the attacking <i>and</i> defending units and press the
    battle marker button (<img src="battle-marker-icon.png"/>) or
    <code>Ctrl-X</code>.  This will place markers on the invovled
    units that identifiy the battle uniquely.</p>

  <p>If the user preference <b>Calculate Odds on battle
    declaration</b> is enabled (default), then the module will also
    calculate the odds (inluding bonuses and penalties for terrain),
    and place an odds marker (e.g., <img width=24 heigh=24
    src="odds_marker_0.png"/>) on one of the involved units.</p>

  <p>The rules says that only <i>one</i> hex may be attacked at a
    time. The module assumes this, but does not enforce it, so be
    careful to not select defending units in more than one hex.</p>

  <table>
    <tr>
      <td>
        <img src="combat_result.png">
      </td>
      <td>
        <img src="combat_implement.png">
      </td>
    </tr>
    <tr>
      <td>
        Resolve combat (<code>Ctrl-Y</code>)
      </td>
      <td>
        Implement result
      </td>
    </tr>
  </table>

  <p>In the factions <b>Combat</b> phase, you can right click the odds
    marker and select <b>Resolve</b> (or <code>Ctrl-Y</code>) to
    resolve the combat.  The module will do the appropriate
    calculations and roll the dices needed, and a replace the odds
    marker with a <i>result marker</i> (e.g. <img width=24 height=24
    src="result_marker_DR.png"/>).</p>

  <p>The module will <i>not</i> apply the result to the invovled
    units. This <i>must</i> be done, according to the rules</i> by the
    factions.</p>

  <p>To clear a battle declaration, select one of the involved units
    or battle markers, and select <b>Clear</b> (or press
    <code>Ctrl-C</code>) from the context menu. All battles can be
    cleared by the <img src="clear-battles-icon.png"/> button in the
    menubar.</p>

  <h4><img src="step_loss.png" width=24 height=24/> &nbsp;Step losses</h4> 

  <p>Use the context menu item <b>Step Loss</b> (or
    <code>Ctrl-F</code>) to implement step losses. Units that are has
    no step losses are automatically eliminated.</p>

  <h2><a name="eliminate"></a>Eliminated units</h2>

  <p>Eliminated units will be sent to a special holding board so that
    the faction may tally the victory points at the end.</p>

  <h2><a name="supply"></a><img src="isolated-icon.png"/> &nbsp;Supply</h2>

  <p>Use the right context menu item <b>Isolated</b> (or
    <code>Ctrl-I</code>) to mark one or more units as isolated.  The
    module <i>cannot</i> automatically detect out-of-supply
    conditions.</p>

  <p>If the optional rule <b>Supply</b> is in force, then the module
    will <i>automatically</i> deduce 1 from an OOS defending unit
    combt factor.</p>

  <h2><a name="detachements"></a><img src="regiment-icon.png"/> &nbsp;Regiment detachements</h2>

  <p>In the OOB window are tabs to keep a record of the detached
    regiments.  Each cell in the tables can be clicked to toggle the
    detached state of a regiment (columns) from a division or brigade
    (rows).  This is <i>not</i> automated in any fashion.  Factions
    should take care to record detachements.</p>


  <h2><a name="about"></a>About this game</h2>

  <p> This VASSAL module was created from L<sup>A</sup>T<sub>E</sub>X
    sources of a Print'n'Play version of the <b>Battle of the
    Bulge</b> game.  That (a PDF) can be found at </p>

  <center>
    <code>https://gitlab.com/wargames_tex/sbotb_tex</code></a>
  </center>

  <p> where this module, and the sources, can also be found.  The PDF
    can also be found as the rules of this game, available in the
    <b>Help</b> menu.  </p>

  <p> The original game was release by the Avalon Hill Game
    Company.</p>

  <h2><a name="changes"></a>Change log</h2>
  <dl>
    <dt>1.0</dt>
    <dd>Initial release of module</dd>
    <dt>1.1</dt>
    <dd>Separate layers for units and markers</dd>
    <dt>1.2</dt>
    <dd>
      <ul>
        <li>Fading drop shadows on units</li>
        <li>Illustrations of combat</li>
      </ul>
    </dd>
  </dl>

  <h2><a name="credits"></a>Credits</a>
  <dl>
    <dt>Design, development &amp; rules:</dt>
    <dd>Samuel Craig Taylor, Jr.</dd>
    <dt>Cover art:</dt><dd>George Parrish</dd>
    <dt>Graphics &amp; typesetting:</dt><dd>Charles Kibler</dd>
    <dt>Packaging:</dt><dd>Monarch Services &amp; Eastern Box</dd>
    <dt>Playtesters:</dt>
    <dd>Robert Coggins, Mike Craighead, Donald Greenwood, Ronald
      LaPorte, Robert Liebel, Rex Martin, William Meyers, George
      Petronis, &amp; Edward Phillips</dd>
  </dl>

  <h2><a name="copyright"></a>Copyright and license</h2>

  <p> This work is &#127279; 2023 Christian Holm Christensen, and
    licensed under the Creative Commons Attribution-ShareAlike 4.0
    International License. To view a copy of this license, visit </p>

  <center>
    <code>http://creativecommons.org/licenses/by-sa/4.0</code>
  </center>

  <p>
    or send a letter to
  </p>

  <center>
    Creative Commons<br>
    PO Box 1866<br>
    Mountain View<br>
    CA 94042<br>
    USA
  </center>
</body>
</html>'''

# --------------------------------------------------------------------
def concatHexes(hexes):
    return ':'+':'.join(hexes) + ':'
# --------------------------------------------------------------------
def getHexes(hexes,what):
    return [k for k,v in hexes.items() if what in v]
# --------------------------------------------------------------------
def getWoodsHexes(hexes):
    return concatHexes(getHexes(hexes,'woods'))
# --------------------------------------------------------------------
def getRoughHexes(hexes):
    return concatHexes(getHexes(hexes,'rough'))
# --------------------------------------------------------------------
def getFortifiedHexes(hexes):
    return concatHexes(getHexes(hexes,'fortified'))
# --------------------------------------------------------------------
def getTownHexes(hexes):
    return concatHexes(getHexes(hexes,'town'))
# --------------------------------------------------------------------
def getFortHexes(hexes):
    return concatHexes(getHexes(hexes,'fort'))
# --------------------------------------------------------------------
def getBritainHexes(hexes):
    return concatHexes(getHexes(hexes,'britain'))
# --------------------------------------------------------------------
def getContinentHexes(hexes):
    return concatHexes([k for k,v in hexes.items() if 'britain' not in v])
# --------------------------------------------------------------------
def getRiverCrossings():
    # These are taken from the 'hexes.tex' file and then some
    # agressive Emacs qiuery-regexp-replace.
    # Meuse
    rivers = [
        ['B12.W',
         'B12.NW',
         'B11.W',
         'B11.NW',
         'B10.W',
         'B10.NW',
         'B10.NE',
         'C9.SW',
         'C9.W',
         'C9.NW',
         'C8.W',
         'C8.NW',
         'C7.W',
         'C7.NW',
         'C6.W',
         'C6.NW',
         'C5.W',
         'C5.NW',
         'C5.NE',
         'D5.NW',
         'D5.NE',
         'E5.NW',
         'E5.NE',
         'F4.NW',    
         'F4.NE',
         'G4.NW',
         'G4.NE',
         'H4.NW',
         'H4.NE',
         'I4.NW',
         'I4.NE',
         'J3.NW',
         'J3.NE', 
         'K3.NW',
         'K3.NE',
         'L2.NW',
         'L2.NE', 
         'M2.NW',
         'M2.NE',
         'M1.E',
         'M1.NE',
         ],
        # NN 
        ['A6.NW',
         'A6.NE',
         'B5.NW',
         'B5.NE',
         ],
        #  
        # Lesse
        ['C10.NW',
         'C10.NE',
         'D10.NW',
         'D10.NE',
         'E11.NW',
         'E11.NE',
         'F11.NW',
         'F11.NE',
         'F11.E',
         'F11.SE',
         'F12.E',
         'F12.SE',
         'F13.E',
         'F13.SE',
         'G15.NW',
         'G15.NE',
         'H15.NW',
         'H15.NE',
         'H15.E',
         ],
        #  
        # Semois
        ['B16.W',
         'B16.NW',
         'B16.NE',
         'C16.NW',
         'C16.NE',
         'C16.E',
         'C17.NE',
         'C17.E',
         'D17.NE',
         'D17.E',
         'E17.SE',
         'F17.SW',
         ],
        #  
        # Vesdre
        ['M2.W',
         'M2.SW',
         'M2.SE',
         'N2.SW',
         'N2.SE',
         'O2.SW',
         'O2.SE',    
         'P2.SW',
         'P2.SE',
         'Q2.SW',
         'Q2.SE',
         'R1.SW',
         'R1.SE',
         'S1.SW',
         'S1.SE',
         'T1.NW',
         'T1.NE',
         ],
        #  
        # Ourthe
        ['M3.NW',
         'M3.W',
         'M3.SW',
         'M4.W',
         'M4.SW',
         'L4.SE',
         'L4.SW',
         'L5.W',    
         'L5.SW',
         'K7.NE',
         'K7.NW',
         'K7.W',
         'K8.NW',
         'K8.W',
         'K8.SW',
         'K8.SE',
         'K9.E',
         'K9.SE',
         'L10.NW',
         'L10.NE',
         'M11.NW',
         'M11.NE',
         'N11.NW',
         'N11.NE',
         'N11.E',
         'N12.NE',
         'N12.NW',
         'M13.NE',
         'M13.NW',
         'L13.NE',
         'L13.NW',
         'L13.W',
         'L13.SW',
         'L14.W',
         'L14.SW',
         ],
        #  
        # Ambleve
        ['N11.NE',
         'O11.NW',
         'O11.NE',
         'P11.NW',
         'P11.NE',
         'Q11.NW',
         'Q11.NE',
         'R10.NW',
         'R10.NE',
         'S10.NW',
         ],
        # NN 
        ['M5.NW',
         'M5.NE',
         'N5.NW',
         'N5.NE',
         'O6.NW',    
         'O6.NE',
         'P6.NW',
         'P6.NE',
         'Q7.NW',
         'Q7.NE',
         'R6.NW',
         'R6.NE',
         'S6.NW',
         'S6.NE',
         'T5.NW',
         'T5.NE',
         'U5.NW',
         'U5.NE',
         'V5.NW',
         'V5.NE',
         'V5.E',
         ],
        #  
        # Salm
        ['Q7.NW',
         'Q7.W',
         'Q7.SW',
         'Q7.SE',
         'Q8.E',
         'Q8.SE',
         ],
        #  NN
        ['S6.W',
         'S6.SW',
         'S6.SE',
         'T6.SW',
         'T6.SE',
         'U6.SW',
         'U6.SE',
         'V6.SW',
         'V6.SE',
         ],
        #  
        # Roer
        ['X1.NE',
         'X1.E',
         'X1.SE',
         'X1.SW',         
         'W2.SE',
         'W2.SW',
         ],
        #  
        # Our
        ['Y1.NW',
         'Y1.NE',
         'Y1.E',
         'Y1.SE',
         'Y2.E',
         ],
        #  
        ['Y17.E',
         'Y17.NE',
         'Y17.NW',
         'X16.NE',
         'X16.NW',
         'W16.NE',
         'W16.NW',
         'W15.W',
         'W15.NW',
         'V13.SE',
         'V13.SW',
         'U13.SE',
         'U13.SW',
         'U13.W',
         'U13.NW',
         'U12.W',
         'U12.NW',    
         'U11.W',
         'U11.NW',
         'U10.W',
         'U10.NW',
         'U10.NE',
         'U9.E',
         'U9.NE',
         'V8.NW',
         'V8.NE',
         'W8.NW',
         'W8.NE',
         'X7.NW',
         'X7.NE',
         ],
        #
        # Clerf
        ['W16.NW',
         'V16.NE',
         'V16.NW',
         'U16.NE',
         'U16.NW',
         'T15.NE',
         'T15.NW',
         'S15.NE',
         'S14.E',
         'S14.NE',
         'S14.NW',
         'S13.W',
         'S13.NW',    
         'S12.W',
         'S12.NW',
         'S12.NE',
         'S11.E',
         ],
        #
        # Sure
        ['T15.NW',
         'S16.NE',
         'S16.NW',
         'R15.NE',
         'R15.NW',
         'Q16.NE',
         'Q16.NW',
         'P16.NE',
         'P16.NW',
         'O17.NE',
         'O17.NW',
         'N16.NE',
         'N16.NW',
         'M16.NE',
         ],
        # NN
        ['S15.NE',
         'S15.NW',
         'R14.NE',
         'R14.NW',
         'Q14.NE',
         'Q14.NW',
         'P13.NE',
         ]
    ]
    edges = {
        ('NW','NE'): ('N',  lambda c,r:(c,r-1)),
        ('NE','E'):  ('NE', lambda c,r:(c+1,r-(c%2))),
        ('E','SE'):  ('SE', lambda c,r:(c+1,r+((c+1)%2))),
        ('SE','SW'): ('S',  lambda c,r:(c,r+1)),
        ('SW','W'):  ('SW', lambda c,r:(c-1,r+((c+1)%2))),
        ('W','NW'):  ('NW', lambda c,r:(c-1,r-(c%2)))}
    samecol = {
        'NW': 'SW',
        'NE': 'SE',
        'SW': 'NW',
        'SE': 'NE' }

    def getEdge(w1,w2):
        return edges.get((w1,w2),edges.get((w2,w1),None))
    def getTarget(edge,c1,r1):
        if edge is None:
            return ''
        targ = edge[1](c1,r1)
        if targ is None:
            return ''
        return hexName(targ[0],targ[1])
    def hexName(c,r):
        return f'{chr(c+ord("A")-1):1s}{r}'

    from re import match

    #foo = open('crossings.tex','w')
    #print(r'\begin{scope}[line width=2pt,red,'
    #      f'dash pattern=on 0mm off 5mm on 10mm off 5mm]',file=foo)
    
    crossing = []
    for river in rivers:
        for v1,v2 in zip(river[:-1],river[1:]):
            # print(f'{v1:6s}-{v2:6s} -> ',end='')
            h1 = v1[:v1.index('.')]
            h2 = v2[:v2.index('.')]
            s1 = match(r'([A-Z])([0-9]+)',h1)
            s2 = match(r'([A-Z])([0-9]+)',h2)
            c1 = ord(s1[1])-ord('A')+1
            c2 = ord(s2[1])-ord('A')+1
            r1 = int(s1[2])
            r2 = int(s2[2])
            w1 = v1[v1.index('.')+1:]
            w2 = v2[v2.index('.')+1:]
            edge = ['?','?']
            targ = '?'
            orig = '?'
            if h1 == h2:
                edge = getEdge(w1,w2)
                if edge is None:
                    raise RuntimeError(f'Edge {w1},{w2} ({v1},{v2}) not found')

                orig = h1
                targ = getTarget(edge,c1,r1)
            else:
                if c1 == c2: # Same column
                    ww2   = samecol.get(w2,None)
                    # print(f'{v1:6s} - {v2:6s} -> {w2}')
                    if ww2 is None:
                        h1 = h2
                        c1, c2 = c2, c1
                        r1, r2 = r2, r1
                        w1, w2 = w2, w1
                        ww2 = samecol[w2]
                    if ww2 is None:
                        raise RuntimeError(f'Same vertex {w2} not found')
                    w2 = ww2
                    edge = getEdge(w1,w2)
                    orig = h1
                    targ = getTarget(edge,c1,r1)
                else:
                    # Make sure we only look right 
                    if c2 < c1:
                        c1, c2 = c2, c1
                        r1, r2 = r2, r1
                        w1, w2 = w2, w1
                        h1, h2 = h2, h1
                        #print(f'<> {h1+"."+w1}-{h2+"."+w2} ', end='')

                    assert w1 != 'W',\
                        f'Invalid first corner {w1}'
                    assert w2 != 'E',\
                        f'Invalid second corner {w2}'

                    # Only take top edge 
                    if w1 in ['SE','SW']:
                        w1 =  samecol[w1]
                        r1 += 1
                        # print(f'[1 -> {hexName(c1,r1)+"."+w1:6s}] ',end=' ')

                    if w2 in ['SE','SW']:
                        w2 =  samecol[w2]
                        r2 += 1
                        # print(f'[2 -> {hexName(c2,r2)+"."+w2:6s}] ',end='')

                    # print(f'({hexName(c1,r1)+"."+w1}-'
                    #       f'{hexName(c2,r2)+"."+w2}) -> ',end='')
                    cc = c2
                    rr = r2
                    if w1 == 'NE':
                        w1  =  'W'
                        if w2 == 'NW' and (c1 % 2 == 0) and r1 != r2:
                            rr -= 1
                            w2 =  samecol[w2]
                        if w2 == 'NW' and (c1 % 2 == 1) and r1 == r2:
                            rr -= 1
                            w2 =  samecol[w2]
                        
                    elif w1 == 'E':
                        w1   = 'NW'
                    elif w1 == 'NW':
                        assert w2 == 'W',\
                            f'w1={w1} but w2={w2}'
                        cc   = c1
                        rr   = r1
                        w2   = 'NE'

                    # print(f'{hexName(cc,rr)} {w1:2s}-{w2:2s} ->',end='')
                    edge = getEdge(w1,w2)
                    orig = hexName(cc,rr)
                    targ = getTarget(edge,cc,rr)
                        
            crossing.append([orig,targ])
            # print(f'Crossing {orig:3s} -> {targ:3s} ({edge[0]})')
            # print(fr'  \draw[->] ({orig})--({targ});'
            #       f' % {v1:6s}--{v2:6s}',file=foo)

    # print(r'\end{scope}',file=foo)
    # foo.close()
    
    return ':'+':'.join([f'{h1}{h2}:{h2}{h1}' for h1,h2 in crossing])+':'

# --------------------------------------------------------------------
def addTurnPrototypes(n,
                      turns,
                      main,
                      prototypesContainer,
                      phaseNames,
                      marker='game turn',
                      zoneName='turns',
                      globalScenario='Scenario'):
    # ----------------------------------------------------------------
    # Loop over turns, and create prototype for game turn marker to
    # move it along the turn track.  We do that by adding commands to
    # the turn track, which then delegates to the prototype.
    #
    # We add the prototypes to the game turn marker later on.
    board = main['mapName']
    turnp = []
    for t in range(1,n+1):
        k = key(NONE,0)+f',Turn{t}'
        turns.addHotkey(hotkey       = k,
                        match        = (f'{{Turn=={t}&&'
                                        f'(Phase=="{phaseNames[0]}"||'
                                        f'Phase=="{phaseNames[1]}")}}'),
                        reportFormat = (f'{{"=== <b>Turn "+({t}+'
                                        f'{globalScenario}==22?6:0)+'
                                        f'"</b> ==="}}'),
                        name         = f'Turn {t}')
        main.addMassKey(name         = f'Turn marker to {t}',
                        buttonHotkey = k,
                        hotkey       = k,
                        buttonText   = '', 
                        target       = '',
                        filter       = (f'{{BasicName == "{marker}"}}'))
        pn     = f'To turn {t}'
        reg    = f'{{{globalScenario}==22?"Turn {t+6}":'
        if t == 1:
            reg += f'"{marker}"'
        else:
            reg += f'"Turn {t}"'
        reg += f'}}'
        traits = [
            SendtoTrait(mapName     = board,
                        boardName   = board,
                        name        = '',
                        restoreName = '',
                        restoreKey  = '',
                        zone        = zoneName,
                        destination = 'R', #R for region
                        region      = reg,
                        key         = k,
                        x           = 0,
                        y           = 0),
            BasicTrait()]
        prototypesContainer.addPrototype(name        = f'{pn} prototype',
                                         description = f'{pn} prototype',
                                         traits      = traits)
        turnp.append(pn)

    return turnp

# --------------------------------------------------------------------
# Our main patching function
def patch(build,data,vmod,gverbose=False):
    from re  import sub
    from PIL import Image
    from io  import BytesIO
    from pathlib import Path
    
    imgs = ['.imgs/combat_situation.png',
            '.imgs/combat_select.png',
            '.imgs/combat_odds.png',
            '.imgs/combat_result.png',
            '.imgs/combat_implement.png'
            ]
    for img in imgs:
        src = Path(img)
        dst = Path('images') / src.name
        vmod.addExternalFile(img,str(dst))
    
    game = build.getGame()

    # These are taken from the 'hexes.tex' file and then some
    # agressive Emacs qiuery-regexp-replace.
    hexes = {
        # --- A ---
        'A1':	'',	#
        'A2':	'',	#
        'A3':	'',	#
        'A4':	'',	#
        'A5':	'',	#
        'A6':	'',	#
        'A7':	'',	#
        'A8':	'',	#
        'A9':	'',	#
        'A10':	'rough',	#
        # 
        # --- B ---
        'B1':	'',	#
        'B2':	'',	#
        'B3':	'',	#
        'B4':	'town',	#
        'B5':	'',	#
        'B6':	'rough',	#
        'B7':	'rough',	#
        'B8':	'',	#
        'B9':	'',	#
        'B10':	'',	#
        'B11':	'rough',	#
        'B12':	'',	#
        'B13':	'',	#
        'B14':	'woods',	#
        'B15':	'woods',	#
        'B16':	'woods',	#
        'B17':	'woods',	#
        # 
        # --- C ---
        'C1':	'',	#
        'C2':	'',	#
        'C3':	'',	#
        'C4':	'',	#
        'C5':	'',	#
        'C6':	'',	#
        'C7':	'woods',	#
        'C8':	'',	#
        'C9':	'town',	#
        'C10':	'',	#
        'C11':	'',	#
        'C12':	'',	#
        'C13':	'woods',	#
        'C14':	'woods town',	#
        'C15':	'',	#
        'C16':	'woods',	#
        'C17':	'town',	#
        #
        # --- D ---
        'D1':	'',	#
        'D2':	'',	#
        'D3':	'',	#
        'D4':	'',	#
        'D5':	'',	#
        'D6':	'woods',	#
        'D7':	'',	#
        'D8':	'',	#
        'D9':	'town',	#
        'D10':	'',	#
        'D11':	'',	#
        'D12':	'',	#
        'D13':	'woods',	#
        'D14':	'',	#
        'D15':	'',	#
        'D16':	'',	#
        'D17':	'woods',	#
        # 
        # --- E ---
        'E1':	'town',	#
        'E2':	'',	#
        'E3':	'',	#
        'E4':	'rough',	#
        'E5':	'',	#
        'E6':	'',	#
        'E7':	'',	#
        'E8':	'town',	#
        'E9':	'',	#
        'E10':	'',	#
        'E11':	'',	#
        'E12':	'',	#
        'E13':	'woods',	#
        'E14':	'',	#
        'E15':	'',	#
        'E16':	'rough',	#
        'E17':	'rough',	#
        # 
        # --- F ---
        'F1':	'',	#
        'F2':	'rough',	#
        'F3':	'',	#
        'F4':	'town',	#
        'F5':	'',	#
        'F6':	'',	#
        'F7':	'',	#
        'F8':	'',	#
        'F9':	'',	#
        'F10':	'woods',	#
        'F11':	'',	#
        'F12':	'',	#
        'F13':	'woods',	#
        'F14':	'',	#
        'F15':	'town',	#
        'F16':	'',	#
        'F17':	'',	#
        # 
        # --- G ---
        'G1':	'',	#
        'G2':	'',	#
        'G3':	'',	#
        'G4':	'town',	#
        'G5':	'',	#
        'G6':	'',	#
        'G7':	'',	#
        'G8':	'',	#
        'G9':	'rough',	#
        'G10':	'woods',	#
        'G11':	'',	#
        'G12':	'',	#
        'G13':	'woods',	#
        'G14':	'',	#
        'G15':	'',	#
        'G16':	'',	#
        'G17':	'woods',	#
        # 
        # --- H ---
        'H1':	'',	#
        'H2':	'',	#
        'H3':	'',	#
        'H4':	'',	#
        'H5':	'',	#
        'H6':	'',	#
        'H7':	'woods',	#
        'H8':	'',	#
        'H9':	'',	#
        'H10':	'town',	#
        'H11':	'',	#
        'H12':	'',	#
        'H13':	'woods',	#
        'H14':	'',	#
        'H15':	'',	#
        'H16':	'',	#
        'H17':	'woods',	#
        # 
        # --- I ---
        'I1':	'',	#
        'I2':	'',	#
        'I3':	'',	#
        'I4':	'',	#
        'I5':	'',	#
        'I6':	'',	#
        'I7':	'',	#
        'I8':	'woods',	#
        'I9':	'town',	#
        'I10':	'',	#
        'I11':	'',	#
        'I12':	'',	#
        'I13':	'',	#
        'I14':	'woods',	#
        'I15':	'',	#
        'I16':	'woods',	#
        'I17':	'woods town',	#
        # 
        # --- J ---
    'J1':	'',	#
        'J2':	'',	#
        'J3':	'',	#
        'J4':	'',	#
        'J5':	'rough',	#
        'J6':	'rough',	#
        'J7':	'',	#
        'J8':	'woods town',	#
        'J9':	'',	#
        'J10':	'',	#
        'J11':	'woods',	#
        'J12':	'',	#
        'J13':	'rough',	#
        'J14':	'',	#
        'J15':	'',	#
        'J16':	'',	#
        'J17':	'woods',	#
        # 
        # --- K ---
        'K1':	'',	#
        'K2':	'',	#
        'K3':	'',	#
        'K4':	'',	#
        'K5':	'',	#
        'K6':	'',	#
        'K7':	'woods',	#
        'K8':	'',	#
        'K9':	'',	#
        'K10':	'',	#
        'K11':	'',	#
        'K12':	'',	#
        'K13':	'woods',	#
        'K14':	'woods',	#
        'K15':	'',	#
        'K16':	'woods',	#
        'K17':	'town',	#
        # 
        # --- L ---
    'L1':	'town',	#
        'L2':	'',	#
        'L3':	'',	#
        'L4':	'',	#
        'L5':	'',	#
        'L6':	'',	#
        'L7':	'',	#
        'L8':	'',	#
        'L9':	'',	#
        'L10':	'town',	#
        'L11':	'',	#
        'L12':	'',	#
        'L13':	'',	#
        'L14':	'woods',	#
        'L15':	'woods',	#
        'L16':	'',	#
        'L17':	'',	#
        # 
        # --- M ---
        'M1':	'',	#
        'M2':	'town',	#
        'M3':	'',	#
        'M4':	'',	#
        'M5':	'',	#
        'M6':	'',	#
        'M7':	'',	#
        'M8':	'woods',	#
        'M9':	'woods',	#
        'M10':	'rough',	#
        'M11':	'',	#
        'M12':	'',	#
        'M13':	'woods',	#
        'M14':	'rough',	#
        'M15':	'',	#
        'M16':	'',	#
        'M17':	'woods',	#
        # 
        # --- N ---
        'N1':	'',	#
        'N2':	'',	#
        'N3':	'',	#
        'N4':	'',	#
        'N5':	'town',	#
        'N6':	'town',	#
        'N7':	'woods town',	#
        'N8':	'',	#
        'N9':	'woods',	#
        'N10':	'woods',	#
        'N11':	'',	#
        'N12':	'',	#
        'N13':	'',	#
        'N14':	'',	#
        'N15':	'rough',	#
        'N16':	'woods',	#
        'N17':	'woods',	#
        # 
        # --- O ---
        'O1':	'',	#
        'O2':	'',	#
        'O3':	'rough',	#
        'O4':	'',	#
        'O5':	'rough',	#
        'O6':	'rough',	#
        'O7':	'woods',	#
        'O8':	'woods',	#
        'O9':	'',	#
        'O10':	'woods',	#
        'O11':	'',	#
        'O12':	'',	#
        'O13':	'',	#
        'O14':	'town',	#
        'O15':	'woods',	#
        'O16':	'woods town',	#
        'O17':	'',	#
        # 
        # --- P ---
    'P1':	'town',	#
        'P2':	'',	#
        'P3':	'rough',	#
        'P4':	'rough',	#
        'P5':	'rough town',	#
        'P6':	'woods town',	#
        'P7':	'woods',	#
        'P8':	'',	#
        'P9':	'woods',	#
        'P10':	'',	#
        'P11':	'',	#
        'P12':	'',	#
        'P13':	'rough',	#
        'P14':	'woods',	#
        'P15':	'woods',	#
        'P16':	'',	#
        'P17':	'',	#
        # 
        # --- Q ---
        'Q1':	'',	#
        'Q2':	'town',	#
        'Q3':	'',	#
        'Q4':	'woods town',	#
        'Q5':	'rough',	#
        'Q6':	'',	#
        'Q7':	'',	#
        'Q8':	'',	#
        'Q9':	'woods',	#
        'Q10':	'',	#
        'Q11':	'',	#
        'Q12':	'',	#
        'Q13':	'rough',	#
        'Q14':	'rough',	#
        'Q15':	'rough',	#
        'Q16':	'woods',	#
        'Q17':	'',	#
        # 
        # --- R ---
        'R1':	'',	#
        'R2':	'woods',	#
        'R3':	'woods',	#
        'R4':	'rough',	#
        'R5':	'rough town',	#
        'R6':	'woods',	#
        'R7':	'town',	#
        'R8':	'woods',	#
        'R9':	'',	#
        'R10':	'',	#
        'R11':	'rough',	#
        'R12':	'',	#
        'R13':	'',	#
        'R14':	'town',	#
        'R15':	'',	#
        'R16':	'',	#
        'R17':	'',	#
        # 
        # --- S ---
        'S1':	'town',	#
        'S2':	'woods',	#
        'S3':	'woods',	#
        'S4':	'rough',	#
        'S5':	'rough',	#
        'S6':	'town',	#
        'S7':	'woods',	#
        'S8':	'woods',	#
        'S9':	'',	#
        'S10':	'rough',	#
        'S11':	'',	#
        'S12':	'rough town',	#
        'S13':	'',	#
        'S14':	'rough',	#
        'S15':	'',	#
        'S16':	'',	#
        'S17':	'woods',	#
        # 
        # --- T ---
        'T1':	'woods',	#
        'T2':	'woods',	#
        'T3':	'rough',	#
        'T4':	'rough',	#
        'T5':	'',	#
        'T6':	'woods',	#
        'T7':	'rough',	#
        'T8':	'',	#
        'T9':	'rough',	#
        'T10':	'',	#
        'T11':	'',	#
        'T12':	'',	#
        'T13':	'',	#
        'T14':	'rough',	#
        'T15':	'town',	#
        'T16':	'woods',	#
        'T17':	'',	#
        # 
        # --- U ---
        'U1':	'woods town',	#
        'U2':	'woods',	#
        'U3':	'woods',	#
        'U4':	'rough',	#
        'U5':	'rough',	#
        'U6':	'',	#
        'U7':	'',	#
        'U8':	'woods',	#
        'U9':	'rough',	#
        'U10':	'rough',	#
        'U11':	'fortified rough',	#
        'U12':	'fortified rough',	#
        'U13':	'fortified rough',	#
        'U14':	'',	#
        'U15':	'woods',	#
        'U16':	'',	#
        'U17':	'',	#
        # 
        # --- V ---
        'V1':	'fortified woods',	#
        'V2':	'town',	#
        'V3':	'woods',	#
        'V4':	'woods town',	#
        'V5':	'',	#
        'V6':	'',	#
        'V7':	'woods',	#
        'V8':	'rough',	#
        'V9':	'rough',	#
        'V10':	'fortified',	#
        'V11':	'rough',	#
        'V12':	'',	#
        'V13':	'fortified rough',	#
        'V14':	'woods',	#
        'V15':	'',	#
        'V16':	'',	#
        'V17':	'',	#
        # 
        # --- W ---
        'W1':	'woods',	#
        'W2':	'fortified rough',	#
        'W3':	'fortified rough',	#
        'W4':	'rough',	#
        'W5':	'town',	#
        'W6':	'rough',	#
        'W7':	'rough',	#
        'W8':	'woods town',	#
        'W9':	'rough',	#
        'W10':	'fortified rough',	#
        'W11':	'',	#
        'W12':	'',	#
        'W13':	'rough',	#
        'W14':	'fortified',	#
        'W15':	'fortified',	#
        'W16':	'',	#
        'W17':	'',	#
        # 
        # --- X ---
        'X1':	'',	#
        'X2':	'',	#
        'X3':	'fortified',	#
        'X4':	'fortified woods',	#
        'X5':	'woods',	#
        'X6':	'woods',	#
        'X7':	'rough',	#
        'X8':	'fortified rough',	#
        'X9':	'fortified',	#
        'X10':	'town',	#
        'X11':	'',	#
        'X12':	'',	#
        'X13':	'',	#
        'X14':	'',	#
        'X15':	'fortified woods',	#
        'X16':	'woods',	#
        'X17':	'woods',	#
        # 
        # --- Y ---
        'Y1':	'',	#
        'Y2':	'',	#
        'Y3':	'woods town',	#
        'Y4':	'',	#
        'Y5':	'fortified',	#
        'Y6':	'fortified woods',	#
        'Y7':	'fortified',	#
        'Y8':	'fortified',	#
        'Y9':	'town',	#
        'Y10':	'',	#
        'Y11':	'',	#
        'Y12':	'',	#
        'Y13':	'',	#
        'Y14':	'',	#
        'Y15':	'',	#
        'Y16':	'fortified',	#
        'Y17':	'town',	#
    }

    # ================================================================
    #
    # Global properties
    #
    # ----------------------------------------------------------------
    gp                = game.getGlobalProperties()[0];
    # ----------------------------------------------------------------
    #
    # Lists of hexes and crossings
    #
    woodsHexes        = getWoodsHexes    (hexes)
    fortifiedHexes    = getFortifiedHexes(hexes)
    townHexes         = getTownHexes     (hexes)
    roughHexes        = getRoughHexes    (hexes)
    riverCrossing     = getRiverCrossings()
    globalWoods       = 'Woods'
    globalFortified   = 'Fortified'
    globalTown        = 'Town'
    globalRough       = 'Rough'
    globalRivers      = 'Rivers'
    
    for h,n in [[woodsHexes,        globalWoods    ],   
                [fortifiedHexes,    globalFortified],   
                [townHexes,         globalTown     ],   
                [roughHexes,        globalRough],   
                [riverCrossing,     globalRivers   ]]:
        gp.addProperty(name         = n,
                       initialValue = h,
                       description  = f'List of {n} hexes')

                         
    # ----------------------------------------------------------------
    globalDeclare   = 'NotDeclare'
    globalResolve   = 'NotResolve'
    globalWeather   = 'GlobalWeather'
    defenderHex     = 'DefenderHex'
    globalNormal    = 'NormalDice'
    globalBiased    = 'BiasedDice'
    globalSetup     = 'NotSetup'
    globalSupremacy = 'AirSupremacy'
    isTutorial      = 'IsTutorial'
    globalScenario  = 'Scenario'
    gp              = game.getGlobalProperties()[0];
    gp.addProperty(name         = defenderHex,
                   initialValue = '',
                   description  = 'Hex of defending unit')
    gp.addProperty(name         = globalDeclare,
                   initialValue = 'true',
                   description  = 'True when not in declare phase')
    gp.addProperty(name         = globalResolve,
                   initialValue = 'true',
                   description  = 'True when not in combat phase')
    gp.addProperty(name         = globalSetup,
                   initialValue = 'false',
                   description  = 'True when not in setup phase')
    gp.addProperty(name         = globalWeather,
                   initialValue = 'Wet',
                   description  = 'True when bad weather')
    gp.addProperty(name         = globalWeather+'German',
                   initialValue = -9,
                   description  = 'True when bad weather')
    gp.addProperty(name         = globalWeather+'Allied',
                   initialValue = -9,
                   description  = 'True when bad weather')
    gp.addProperty(name         = isTutorial,
                   initialValue = str(doTutorial).lower(), 
                   description  = 'Is tutorial loaded')
    gp.addProperty(name         = globalNormal,
                   initialValue = str(False).lower(), 
                   description  = 'If true, disable normal dice')
    gp.addProperty(name         = globalBiased,
                   initialValue = str(True).lower(), 
                   description  = 'Is true, disable biased dice')
    gp.addProperty(name         = globalScenario,
                   initialValue = 16,
                   description  = 'Select setup scenario')
    gp.addProperty(name         = globalSupremacy,
                   initialValue = 'No',
                   description  = 'Current air supremacy')
   
    # ================================================================
    #
    # Options
    #
    # ----------------------------------------------------------------
    optInitiative   = 'optInitiative'
    optdeSF         = 'optdeSF'
    optHeydte       = 'optHeydte'
    optOneFiveZero  = 'optOneFiveZero'
    optWeather      = 'optWeather'
    optAir          = 'optAir'
    optSupply       = 'optSupply'
    optFuel         = 'optFuel'
    optDetachments  = 'optDetachments'
    optRiver        = 'optRiver'
    optPrepared     = 'optPrepared'
    optBritish      = 'optBritish'
    for o,t in [[optInitiative  , 'Initiative'],
		[optdeSF        , 'German Special Forces'],
		[optHeydte      , 'German air drop'],
		[optOneFiveZero , 'German 150th armoured brigade'],
		[optWeather     , 'Weather'],
		[optAir         , 'Air missions'],
		[optSupply      , 'Supply for both factions'],
		[optFuel        , 'German fuel requirements'],
		[optDetachments , 'Regiment detachments'],
		[optRiver       , 'Opposed river crossings'],
		[optPrepared    , 'Prepared positions'],
		[optBritish     , 'Limited British assaults']]:
        gp.addProperty(name         = o,
                       initialValue = 'true',
                       description  = t)

    # ================================================================
    #
    # Keys, global properties
    #
    # ----------------------------------------------------------------
    updateHex      = key(NONE,0)+',updateHex'
    checkDeclare   = key(NONE,0)+',checkDeclare'
    checkResolve   = key(NONE,0)+',checkResolve'
    checkSetup     = key(NONE,0)+',checkSetup'
    returnToBase   = key(NONE,0)+',returnToBase'
    calcOddsKey    = key(NONE,0)+',wgCalcBattleOdds'
    calcFracKey    = key(NONE,0)+',wgCalcBattleFrac'
    calcAF         = key(NONE,0)+',wgCalcBattleAF'
    calcDF         = key(NONE,0)+',wgCalcBattleDF'
    dieRoll        = key(NONE,0)+',dieRoll'
    dicesRoll      = key(NONE,0)+',dicesRoll'
    setWeather     = key(NONE,0)+',setWeather'
    rollWeather    = key(NONE,0)+',rollWeather'
    flipTurn       = key(NONE,0)+',flipTurn'
    startAllied    = key(NONE,0)+',startAllied'    
    startGerman    = key(NONE,0)+',startGerman'
    startInitiative= key(NONE,0)+',startInitiative'
    resolveKey     = key('Y')
    declareKey     = key('X')
    toggleKey      = key(NONE,0)+',toggleOpt'
    flipKey        = key(NONE,0)+',flipOpt'
    setOptional    = key(NONE,0)+',setOpt'
    cleanOptional  = key(NONE,0)+',cleanOpt'
    fixOptional    = key(NONE,0)+',fixOpt'
    deleteKey      = key(NONE,0)+',delete'
    optKey         = key('Q',ALT)
    optShow        = key(NONE,0)+',optShow'
    incrFuelKey    = key(NONE,0)+',incrFuel'
    decrFuelKey    = key(NONE,0)+',decrFuel'
    turnFuelKey    = key(NONE,0)+',turnFuel'
    updateFuelKey  = key(NONE,0)+',updateFuel'
    fixedFuelKey   = key(NONE,0)+',fixedFuel'
    resetFuelKey   = key(NONE,0)+',resetFuel'
    setGerman      = key(NONE,0)+',setGerman'
    setAllied      = key(NONE,0)+',setAllied'
    setNone        = key(NONE,0)+',setNone'
    nextPhase      = key('T',ALT) # key(NONE,0)+',nextPhase'
    resetStep      = key(NONE,0)+',resetStep'
    toggleStep     = key(NONE,0)+',toggleStep'
    toggleBias     = key(NONE,0)+',toggleBias'
    stepKey        = key('F')
    lossKey        = key('L')
    eliminateKey   = key('E')
    eliminateCmd   = key(NONE,0)+',eliminate'
    resetIsolated  = key(NONE,0)+',resetIsolated'
    stepIsolated   = key(NONE,0)+',stepIsolated'
    stepOrElim     = key(NONE,0)+',stepOrElim'
    curDecrKey     = key(NONE,0)+',decrCurrent'
    isolatedKey    = key('I')
    insupplyKey    = key('I',CTRL_SHIFT)
    activateKey    = key('A')
    deactivateKey  = key('A',CTRL_SHIFT)
    clearActivateKey = key(NONE,0)+',clearActivate'
    restoreKey     = key('R')
    tutorialKey    = key(NONE,0)+',isTutorial'
    resolveAir     = key(NONE,0)+',resolveAir'
    debug          = 'wgDebug'
    verbose        = 'wgVerbose'
    hidden         = 'wg hidden unit'
    currentBattle  = 'wgCurrentBattle'
    battleUnit     = 'wgBattleUnit'
    battleCalc     = 'wgBattleCalc'
    battleMarker   = 'wgBattleMarker'
    battleCtrl     = 'wgBattleCtlr'
    battleNo       = 'wgBattleNo'
    battleResult   = 'wgBattleResult'
    battleShift    = 'wgBattleShift'
    battleFrac     = 'wgBattleFrac'
    battleAF       = 'wgBattleAF'
    battleDF       = 'wgBattleDF'
    current        = 'Current'
    stackDx        = 8 # 'wgStackDx'
    stackDy        = 12 # 'wgStackDy'
    start16Key     = key(NONE,0)+',start16'
    start22Key     = key(NONE,0)+',start22'
    setitupKey     = key(NONE,0)+',setitup'
    
    # ================================================================
    #
    # Default preferences
    #
    go    = game.getGlobalOptions()[0]
    prefs = go.getPreferences()
    prefs[debug]          ['default'] = False
    prefs[verbose]        ['default'] = False
    prefs['wgAutoOdds']   ['default'] = True
    prefs['wgAutoResults']['default'] = True
            
    # ================================================================
    #
    # Inventory
    #
    # ----------------------------------------------------------------
    # Add an inventory sorted on the unit supply and isolation status. 
    filt = '{Faction=="Allied"||Faction=="German"}'
    grp  = 'Faction,Step_Level,Isolated_Level'
    disp = ('{PropertyValue==Faction ? Faction : '
            '(Step_Level==1 ? "Full" : "Reduced")+" "+'
            '(Isolated_Level==1 ? "In supply" : '
            'Isolated_Level==2 ? "Out of supply" : "")}')
    game.addInventory(include       = filt,
                      groupBy       = grp,
                      sortFormat    = '$PieceName$',
                      tooltip       = 'Show supply status of pieces',
                      nonLeafFormat = disp,
                      zoomOn        = True,
                      hotkey        = key('S',ALT),
                      refreshHotkey = key('S',ALT_SHIFT),
                      icon          = 'isolated-icon.png')
        
    # ================================================================
    #
    # Dice
    #
    # ----------------------------------------------------------------
    # Dice
    diceName  = '1d10Dice'
    dicesName = '2d10'
    diceKey   = key('0',ALT)
    dicesKey  = key('2',ALT)
    dices     = game.getSymbolicDices()
    for dn, dice in dices.items():
        if dn == '1d10Dice':
            dice['hotkey'] = diceKey
        else:
            dice['hotkey'] = dicesKey

    game.getPieceWindows()['Counters']['icon'] = 'unit-icon.png'
    
    # ----------------------------------------------------------------
    prototypeContainer = game.getPrototypes()[0]
    # ----------------------------------------------------------------
    maps = game.getMaps()
    main = maps['Board']

    # ----------------------------------------------------------------
    # Delete key
    dkey = main.getMassKeys().get('Delete',None)
    if dkey is not None:
        dkey['icon'] = ''

    # ----------------------------------------------------------------
    pieceLayer      = 'PieceLayer'
    unitLayer       = 'Units'
    btlLayer        = 'Units' #'Battle' - doesn't work with auto odds
                              #if not same as units - anyways, the
                              #markers should stack with units.
    oddLayer        = 'Odds'
    resLayer        = 'Result'
    fuelLayer       = 'Fuel depots'
    bridgeLayer     = 'Bridges'

    layerNames = {bridgeLayer: {'t': bridgeLayer, 'i': ''},
                  unitLayer:   {'t': unitLayer, 'i': '' },
                  oddLayer:    {'t': oddLayer+' markers', 'i': ''},
                  resLayer:    {'t': resLayer+' markers', 'i': ''} }
                  
    layers = main.addLayers(description='Layers',
                            layerOrder=layerNames)
    lmenu = main.addMenu(description = 'Toggle layers',
                         text        = '',
                         tooltip     = 'Toggle display of layers',
                         icon        = 'layer-icon.png',
                         menuItems   = ([f'Toggle {ln["t"]}'
                                         for ln in layerNames.values()]
                                        +['Show all']))
    hideP  = main.getHidePiecesButton()
    if len(hideP) > 0:
        print('Found hide pieces button, moving layers before that')
        main.remove(layers)
        main.remove(lmenu)
        main.insertBefore(layers,hideP[0])
        main.insertBefore(lmenu, hideP[0])
    else:
        print('Hide pueces button not found',hideP)
        
    for lt,ln in layerNames.items():
        layers.addControl(name       = lt,
                          tooltip    = f'Toggle display of {ln}',
                          text       = f'Toggle {ln["t"]}',
                          icon       = ln['i'],
                          layers     = [lt])
    layers.addControl(name    = 'Show all',
                      tooltip = f'Show all',
                      text    = f'Show all',
                      command = LayerControl.ENABLE,
                      layers  = list(layerNames.keys()))

    
    # ----------------------------------------------------------------
    # Change stacking to be more tight since stacks may grow high
    stackmetrics = main.getStackMetrics(single=True)[0]
    stackmetrics['unexSepX'] = stackDx
    stackmetrics['unexSepY'] = stackDy
    stackmetrics['exSepX']   = int(1.5*stackDx)
    stackmetrics['exSepY']   = int(1.5*stackDy)
    main['color']            = rgb(255,215,79)
    
    # ----------------------------------------------------------------
    # Extra documentation 
    doc  = game.getDocumentation()[0]
    doc.addHelpFile(title='More information',fileName='help/more.html')
    vmod.addFile('help/more.html', moreHelp.format(title=game['name']))

    # ================================================================
    #
    # Boards, etc.
    #
    # ----------------------------------------------------------------
    # Get the restore global key, set icon and remove the deadmap map,
    # since we will move eliminated units to the OOBs instead.
    unelimKey               = key('E',CTRL_SHIFT)
    restore                 = maps['DeadMap'].getMassKeys()['Restore']
    # restore['icon']         = 'restore-icon.png'
    restore['hotkey']       = unelimKey
    restore['buttonHotkey'] = unelimKey
    oobs                    = game.getChartWindows()['OOBs']
    oobs['icon']            = 'oob-icon.png'
    # game.remove(maps['DeadMap'])
    main.append(restore)

    # ----------------------------------------------------------------
    # Get optionals map
    opts = maps['Optionals']
    opts['allowMultiple'] = False
    opts['markMoved'] = 'Never'
    opts['hotkey']    = optKey
    opts['launch']    = True
    opts['icon']      = 'opt-icon.png'
    opts.remove(opts.getImageSaver()[0])
    opts.remove(opts.getTextSaver()[0])
    opts.remove(opts.getGlobalMap()[0])
    opts.remove(opts.getHidePiecesButton()[0])
    obrd = opts.getBoardPicker()[0].getBoards()['Optionals']
    # print(obrd['width'],obrd['height'])
    # obrd['width']  = 500
    # obrd['height'] = 800
    okeys = opts.getMassKeys()
    # print(okeys)
    for kn,k in okeys.items():
        # print(f'Removing {kn} from Optionals')
        opts.remove(k) 
    ostart = opts.getAtStarts(False)
    for at in ostart:
        # print(at['name'])
        if at['name'] == hidden:
            # print(f'Removing {at["name"]} from Optionals')
            opts.remove(at)

    game.addStartupMassKey(name        = 'Optionals',
                           hotkey      = optKey,
                           target      = '',
                           filter      = f'{{BasicName=="{hidden}"}}',
                           whenToApply = StartupMassKey.START_GAME,
                           reportFormat=
                           f'{{{debug}?("Show optionals window"):""}}')
    game.addStartupMassKey(name        = 'Tutorial',
                           hotkey      = tutorialKey,
                           target      = '',
                           filter      = f'{{BasicName=="{hidden}"}}',
                           whenToApply = StartupMassKey.START_GAME,
                           reportFormat=
                           f'{{{debug}?("Show notes window "+{isTutorial}):""}}'                           )
    
    # ----------------------------------------------------------------
    # Extract units on the OOB so we may remove units we do not need
    def stripAt(s):
        try:
            return s[:s.index('@')]
        except:
            return s
        
    # ----------------------------------------------------------------
    # Set custom icons on some global keys 
    mkeys = main.getMassKeys()
    mkeys['Eliminate']['icon'] = 'eliminate-icon.png'
    mkeys['Flip']     ['icon'] = 'flip-icon.png'

    # calcOddsAuto           = mkeys['Calc battle odds']
    # calcOddsAuto['filter'] = '{Phase.contains("combat")&&wgBattleCalc==true}'
    
    # ----------------------------------------------------------------
    # Get Zoned area
    zoned = main.getBoardPicker()[0].getBoards()['Board'].getZonedGrids()[0]

    # ----------------------------------------------------------------
    # Adjust the hexes
    #
    # Get the hex grid and turn of drawing
    zones  = zoned.getZones()
    hzone  = zones['hexes']
    hgrids = hzone.getHexGrids()
    hgrid  = hgrids[0]
    hnum   = hgrid.getNumbering()[0]
    #print(f'grid offsets: h={hnum["hOff"]} v={hnum["vOff"]}')
    hnum['hOff']  =  0
    hnum['vOff']  = int(hnum['vOff']) - 1

    # ----------------------------------------------------------------
    # Adjust the hexes
    #
    # Get the hex grid and turn of drawing

    # ================================================================
    #
    # Turn track
    #
    # ----------------------------------------------------------------
    turns         = game.getTurnTracks()['Turn']
    phaseNames    = ['Setup',                           # 0
                     'Weather',                         # 1
                     'Air unit allocation',             # 2
                     'Allied air drops (1)',            # 3,
                     'German activation',               # 4,
                     'Allied interdictions',            # 5,
                     'German movement',                 # 6,
                     'German combat',                   # 7,
                     'Supply (1)',                      # 8,
                     'Allied air drops (2)',            # 9,
                     'Allied movement',                 # 10,
                     'Allied combat',                   # 11,
                     'Supply (2)'                       # 12
                     ]
    germanAct      = phaseNames.index('German activation')
    alliedDrop2    = phaseNames.index('Allied air drops (2)')
    alliedSup      = phaseNames.index('Supply (2)')
    germanSup      = phaseNames.index('Supply (1)')
    phases         = turns.getLists()['Phase']
    phases['list'] = ','.join(phaseNames)
    turns['reportFormat'] = '--- <b><i>$newTurn$</i></b> ---'

    # ----------------------------------------------------------------
    # Turn track progress 
    turnp = addTurnPrototypes(16,turns,main,prototypeContainer,phaseNames,
                              marker='game turn', zoneName='Turn track',
                              globalScenario=globalScenario)

    # ----------------------------------------------------------------
    # Specific actions 
    turns.addHotkey(hotkey       = fixOptional,
                    match        = f'{{Phase=="{phaseNames[1]}"&&Turn==1}}',
                    reportFormat = (f'{{{debug}?"~Cleaning for optional rules":'
                                    f'""}}'),
                    name         = 'CleanOptional')
    turns.addHotkey(hotkey       = setitupKey,
                    match        = f'{{Phase=="{phaseNames[1]}"&&Turn==1}}',
                    reportFormat = (f'{{{debug}?"~Cleaning for optional rules":'
                                    f'""}}'),
                    name         = 'SetItUp')
    turns.addHotkey(hotkey       = setitupKey,
                    match        = f'{{Phase=="{phaseNames[1]}"&&Turn==1&&{optdeSF}}}',
                    reportFormat = (f'{{{verbose}?"`Remember to place German SF units":'
                                    f'""}}'),
                    name         = 'Reminder')
    turns.addHotkey(hotkey       = flipTurn,
                    match        = f'{{Phase=="{phaseNames[germanAct]}"}}',
                    reportFormat = '--- <b>German Turn</b> ---',
                    name         = 'German Turn')
    turns.addHotkey(hotkey       = flipTurn,
                    match        = f'{{Phase=="{phaseNames[alliedDrop2]}"}}',
                    reportFormat = '--- <b>Allied Turn</b> ---',
                    name         = 'Allied Turn')
    turns.addHotkey(hotkey       = rollWeather,
                    match        = f'{{Phase=="{phaseNames[1]}"}}',
                    reportFormat = (f'{{{verbose}?("--- <b>Weather</b>: <i>"+'
                                    f'{globalWeather}+"</i>---"):""}}'),
                    name         = 'Resolve weather')
    turns.addHotkey(hotkey       = resolveAir,
                    match        = f'{{Phase=="{phaseNames[2]}"}}',
                    reportFormat = (f'{{{verbose}?("`<i>"+'
                                    f'{globalSupremacy}+'
                                    f'" faction has air supremacy</i>"):""}}'),
                    name         = 'Resolve air units')
    turns.addHotkey(hotkey       = turnFuelKey+'German',
                    match        = (f'{{Phase=="{phaseNames[germanAct]}"}}'),
                    reportFormat = f'{{{debug}?"~ Increment German Fuel":""}}',
                    name         = 'Add German fuel resources')
    turns.addHotkey(hotkey       = clearActivateKey,
                    match        = (f'{{{optFuel}&&Phase=="Supply (1)"}}'),
                    reportFormat = f'{{{debug}?("Remove activations"):""}}',
                    name         = 'Remove activations')
    # --- Add commands to turn track to do stuff, and provide reminders
    turns.addHotkey(hotkey = checkDeclare,
                    name   = 'Check for (any) declare phase')
    turns.addHotkey(hotkey = checkResolve,
                    name   = 'Check for (any) combat phase')
    turns.addHotkey(hotkey = checkSetup,
                    match  = '{Turn==1}',
                    name   = 'Check for setup phase')
    turns.addHotkey(hotkey = nextPhase,
                    match  = '{Phase=="Setup"&&Turn>1}',
                    name   = 'Skip setup phase on later turns')
    # --- Command to return stuff ------------------------------------
    turns.addHotkey(hotkey = returnToBase,
                    match  = f'{{Phase=="{phaseNames[-1]}"}}',
                    name   = 'Return air units to OOB')
    # --- Skip phases ------------------------------------------------
    skipPhases = {optWeather: ['Weather'],
                  optAir:     ['Air unit','Allied air drop','Allied interdict'],
                  optFuel:    ['German activation'],
                  optSupply:  ['Supply (1)']}
    for opt, phases in skipPhases.items():
        for phase in phases:
            turns.addHotkey(hotkey = nextPhase,
                            name   = f'Skip {phase} if not {opt}',
                            match  = (f'{{{opt}==false&&'
                                      f'Phase.contains("{phase}")}}'))
    # --- Clear markers phases ---------------------------------------
    keys = turns.getHotkeys(asdict=True)
    keys['Clear battle markers']['match'] = (
        f'{{Phase=="German movement"'
        f'||Phase.contains("Supply")'
        f'||Phase=="Allied movement"'
        f'}}')

    # ================================================================
    #
    # Global key commands
    #
    # ----------------------------------------------------------------
    curBtl        = (f'{{{battleNo}=={currentBattle}&&'
                     f'{battleUnit}==true}}')
    curDef        = (f'{{{battleNo}=={currentBattle}&&'
                     f'{battleUnit}==true&&'
                     f'IsAttacker==false}}')
    curAtt        = (f'{{{battleNo}=={currentBattle}&&'
                     f'{battleUnit}==true&&'
                     f'IsAttacker==true}}')
    keys                     = main.getMassKeys()
    userMark                 = keys['User mark battle']
    selMark                  = keys['Selected mark battle']
    resMark                  = keys['Selected resolve battle']
    userMark['canDisable']   = True
    userMark['propertyGate'] = globalDeclare
    selMark ['canDisable']   = True
    selMark ['propertyGate'] = globalDeclare
    resMark ['canDisable']   = True
    resMark ['propertyGate'] = globalResolve
    
    # ----------------------------------------------------------------
    # Add global key to set isolated status 
    main.addMassKey(name         = 'Add isolated marker',
                    buttonHotkey = isolatedKey,
                    hotkey       = isolatedKey,
                    buttonText   = '', # g['name']
                    icon         = 'isolated-icon.png',
                    reportSingle = True,
                    singleMap    = True,
                    tooltip      = 'Add isolated marker')
    main.addMassKey(name         = 'In supply',
                    buttonHotkey = insupplyKey,
                    hotkey       = insupplyKey,
                    buttonText   = '', # g['name']
                    icon         = '',
                    reportSingle = True,
                    singleMap    = True)
    # ----------------------------------------------------------------
    # Add global key to set isolated status 
    main.addMassKey(name         = 'Add activation marker',
                    buttonHotkey = activateKey,
                    hotkey       = activateKey,
                    buttonText   = '', # g['name']
                    icon         = 'activate-icon.png',
                    reportSingle = True,
                    singleMap    = True,
                    tooltip      = 'Add activate marker')
    main.addMassKey(name         = 'De-activate',
                    buttonHotkey = deactivateKey,
                    hotkey       = deactivateKey,
                    buttonText   = '', # g['name']
                    icon         = '',
                    reportSingle = True,
                    singleMap    = True)
    main.addMassKey(name         = 'De-activate',
                    buttonHotkey = clearActivateKey,
                    hotkey       = deactivateKey,
                    buttonText   = '', # g['name']
                    target       = '',
                    icon         = '',
                    reportSingle = True,
                    singleMap    = True)
    # ----------------------------------------------------------------
    # Add some global keys to the map
    #
    # - Flip turn marker (Ctrl+Shift+F)
    # - Mark twice out-of-supply Allied units (Ctrl+Shift+I)
    # - Eliminate twice isolated Allied units (Ctrl+Shift+E)
    main.addMassKey(name         = 'Flip turn marker',
                    buttonHotkey = flipTurn,
                    hotkey       = stepKey,
                    buttonText   = '', 
                    target       = '',
                    singleMap    = True,
                    filter       = ('{BasicName == "game turn"}'))
    # ----------------------------------------------------------------
    # - Historical setups
    main.addMassKey(name         = 'Dec 16 setup',
                    icon         = '',#'german-icon.png',
                    buttonHotkey = start16Key, #key('G',CTRL_SHIFT),
                    hotkey       = start16Key,
                    buttonText   = '',
                    target       = '',
                    singleMap    = False,
                    canDisable   = False,
                    #propertyGate = '{{Phase=="Setup"}}',
                    tooltip      = 'Load 16th of December setup',
                    reportFormat = 'Moving all units to start-up')
    main.addMassKey(name         = 'Dec 22 setup',
                    icon         = '',#'allied-icon.png',
                    buttonHotkey = start22Key, #key('A',CTRL_SHIFT),
                    hotkey       = start22Key,
                    buttonText   = '',
                    target       = '',
                    singleMap    = False,
                    canDisable   = False,
                    #propertyGate = '{{Phase=="Setup"}}',
                    tooltip      = 'Load 22nd of December setup',
                    reportFormat = 'Moving all units to start-up')
    # ----------------------------------------------------------------
    # - Check for combat phase 
    main.addMassKey(name         = 'Check for declare phase',
                    buttonHotkey = checkDeclare,
                    buttonText   = '',
                    hotkey       = checkDeclare,
                    singleMap    = True,
                    target       = '',
                    filter       = f'{{BasicName=="{hidden}"}}',
                    reportFormat = (f'{{{debug}?("~ Check for declare"):""}}'))
    # ----------------------------------------------------------------
    # - Check for combat phase 
    main.addMassKey(name         = 'Return air units to base',
                    buttonHotkey = returnToBase,
                    buttonText   = '',
                    hotkey       = eliminateKey,
                    singleMap    = True,
                    target       = '',
                    filter       = f'{{Type=="fixed wing"}}',
                    reportFormat = (f'{{{debug}?("~ Return air units"):""}}'))
    # ----------------------------------------------------------------
    # - Check for combat phase 
    main.addMassKey(name         = 'Check for combat phase',
                    buttonHotkey = checkResolve,
                    buttonText   = '',
                    hotkey       = checkResolve,
                    singleMap    = True,
                    target       = '',
                    filter       = f'{{BasicName=="{hidden}"}}',
                    reportFormat = (f'{{{debug}?("~ Check for resolve"):""}}'))
    # ----------------------------------------------------------------
    # - Store defender hex 
    main.addMassKey(name         = 'Store defender hex',
                    buttonHotkey = updateHex,
                    buttonText   = '',
                    singleMap    = True,
                    hotkey       = updateHex,
                    target       = '',
                    filter       = curDef,
                    reportFormat = (f'{{{debug}?("Mass update defender hex -"'
                                    f'+{defenderHex}+"-"):""}}'))
    # ----------------------------------------------------------------
    main.addMassKey(name         = 'German Turn Fuel',
                    buttonHotkey = turnFuelKey+'German',
                    hotkey       = turnFuelKey,
                    buttonText   = '',
                    icon         = '',
                    reportSingle = True,
                    reportFormat = '',
                    target       = '',
                    singleMap    = False,
                    filter       = (f'{{BasicName == "de fuel"&&'
                                    f'Phase.contains("German")}}'),
                    tooltip      = 'Increment Fuel counter')
    # ----------------------------------------------------------------
    # - Roll for weather 
    main.addMassKey(name         = 'Roll for weather',
                    buttonHotkey = rollWeather,
                    buttonText   = '',
                    hotkey       = rollWeather,
                    singleMap    = True,
                    target       = '',
                    filter       = '{BasicName=="weather"}')
    # ----------------------------------------------------------------
    # - Resolve air units 
    main.addMassKey(name         = 'Resovle air units',
                    buttonHotkey = resolveAir,
                    buttonText   = '',
                    hotkey       = resolveAir,
                    singleMap    = True,
                    target       = '',
                    filter       = '{BasicName=="weather"}')
    # ----------------------------------------------------------------
    # - Clean counters based on optional flags 
    main.addMassKey(name         = 'Clean optional initiative',
                    buttonHotkey = fixOptional,
                    buttonText   = '',
                    hotkey       = fixOptional,
                    singleMap    = True,
                    target       = '',
                    filter       = (f'{{BasicName=="weather"}}'),
                    reportFormat = (f'{{{debug}?("~ Fix up optionals"):""}}'))
    main.addMassKey(name         = 'Set it up',
                    buttonHotkey = setitupKey,
                    buttonText   = '',
                    hotkey       = setitupKey,
                    singleMap    = False,
                    target       = '',
                    filter       = (f'{{BasicName=="weather"}}'),
                    reportFormat = (f'{{{debug}?("~ Set-up pieces"):""}}'))
    main.addMassKey(name         = 'Clean optional initiative',
                    buttonHotkey = cleanOptional,
                    buttonText   = '',
                    hotkey       = deleteKey,
                    singleMap    = False,
                    target       = '',
                    filter       = (f'{{{optInitiative}==false&&'
                                    f'BasicName=="initiative"}}'),
                    reportFormat = (f'{{{debug}?("~ Clear initiative "+'
                                    f'({optInitiative}?"no":"yes")):""}}'))
    main.addMassKey(name         = 'Clean optional air',
                    buttonHotkey = cleanOptional,
                    buttonText   = '',
                    hotkey       = deleteKey,
                    singleMap    = False,
                    target       = '',
                    filter       = (f'{{{optAir}==false&&'
                                    f'Type=="fixed wing"}}'),
                    reportFormat = (f'{{{debug}?("~ Clear air "+'
                                    f'({optAir}?"no":"yes")):""}}'))
    main.addMassKey(name         = 'Clean optional fuel',
                    buttonHotkey = cleanOptional,
                    buttonText   = '',
                    hotkey       = deleteKey,
                    singleMap    = False,
                    target       = '',
                    filter       = (f'{{{optFuel}==false&&'
                                    f'(BasicName=="de fuel"||'
                                    f'BasicName.contains("depot"))}}'),
                    reportFormat = (f'{{{debug}?("~ Clear Fuel "+'
                                    f'({optFuel}?"no":"yes")):""}}'))
    main.addMassKey(name         = 'Clean optional SF',
                    buttonHotkey = cleanOptional,
                    buttonText   = '',
                    hotkey       = deleteKey,
                    singleMap    = False,
                    target       = '',
                    filter       = (f'{{{optdeSF}==false&&'
                                    f'Type=="SF"}}'),
                    reportFormat = (f'{{{debug}?("~ Clear SF "+'
                                    f'({optdeSF}?"no":"yes")):""}}'))
    main.addMassKey(name         = 'Clean optional Heydte',
                    buttonHotkey = cleanOptional,
                    buttonText   = '',
                    hotkey       = deleteKey,
                    singleMap    = False,
                    target       = '',
                    filter       = (f'{{{optHeydte}==false&&'
                                    f'BasicName=="de heydte abir"}}'),
                    reportFormat = (f'{{{debug}?("~ Clear Heydte "+'
                                    f'({optHeydte}?"no":"yes")):""}}'))
    main.addMassKey(name         = 'Clean optional prepared',
                    buttonHotkey = cleanOptional,
                    buttonText   = '',
                    hotkey       = deleteKey,
                    singleMap    = False,
                    target       = '',
                    filter       = (f'{{{optPrepared}==false&&'
                                    f'BasicName.contains("prepared")}}'),
                    reportFormat = (f'{{{debug}?("~ Clear prepared positions "+'
                                    f'({optPrepared}?"no":"yes")):""}}'))
    main.addMassKey(name         = 'Clean unused units',
                    buttonHotkey = cleanOptional,
                    buttonText   = '',
                    hotkey       = deleteKey,
                    singleMap    = False,
                    target       = '',
                    filter       = (f'{{UnitScenario!={globalScenario}}}'),
                    reportFormat = (f'{{{debug}?("~ Clear unused units"):""}}'))
    
    # ================================================================
    #
    # Prototypes
    #
    # ----------------------------------------------------------------
    # Clean up prototypes
    prototypes         = prototypeContainer.getPrototypes(asdict=False)
    seen       = list()
    for p in prototypes:
        if p['name'] == ' prototype':
            prototypes.remove(p)
        if p['name'] in seen:
            # print(f'Removing prototype {p["name"]}')
            prototypes.remove(p)
        seen.append(p['name'])
            

    prototypes     = prototypeContainer.getPrototypes(asdict=True)
    # ----------------------------------------------------------------
    # Modify Markers prototype
    markersP     = prototypes['Markers prototype']
    traits       = markersP.getTraits()
    mdel         = Trait.findTrait(traits,DeleteTrait.ID)
    mdel['key']  = deleteKey
    mdel['name'] = ''        
    markersP.setTraits(*traits)
    
    # ----------------------------------------------------------------
    # Modify odds prototype
    oddsP          = prototypes['OddsMarkers prototype']
    traits         = oddsP.getTraits()
    basic          = traits.pop()
    die            = Trait.findTrait(traits,CalculatedTrait.ID,
                                     key='name',value='Die')
    roll           = Trait.findTrait(traits,GlobalHotkeyTrait.ID,
                                     key='globalHotkey',
                                     value=key('6',ALT))
    res           = Trait.findTrait(traits,CalculatedTrait.ID,
                                     key='name',value='BattleResult')
    trg           = Trait.findTrait(traits,TriggerTrait.ID,
                                     key='key',value=resolveKey)
    reg           = (f'(Die+{battleFrac})>=7?"D2":'
                     f'(Die+{battleFrac})>=4?"D1":'
                     f'(Die+{battleFrac})>=1?"DR":'
                     f'(Die+{battleFrac})>=-3?"-":'
                     f'(Die+{battleFrac})>=-6?"A1":"A2"')
    rep            = None

    for t in traits:
        if t.ID != ReportTrait.ID: continue
        if not t['report'].startswith('{"` Battle # '): continue
        rep = t
        break

    roll['globalHotkey'] = dicesKey
    getRoll              = f'GetString("{dicesName}_result")'
    getAlRoll            = 'AlliedDice_result'
    getDeRoll            = 'GermanDice_result'
    getRoll1             = f'(Phase.contains("Allied")?{getAlRoll}:{getDeRoll})'
    getRoll2             = f'(Phase.contains("Allied")?{getDeRoll}:{getAlRoll})'
    parseInt             = 'Integer.parseInt'
    die['expression'] = (f'{{{parseInt}({getRoll1})-{parseInt}({getRoll2})}}')
    die['expression'] = (f'{{{getRoll1}-{getRoll2}}}')
    res['expression'] = (f'{{({reg})}}')

    traits.extend([
        CalculatedTrait(name       = 'Die1',
                        # expression = f'{{{parseInt}({getAlRoll})}}',
                        expression  = f'{{{getAlRoll}}}',
                        description = 'Take first die'),
        RestrictCommandsTrait(
            name          = 'Restrict Ctrl-Y to combat',
            hideOrDisable = RestrictCommandsTrait.DISABLE,
            expression    = (f'{{{globalResolve}==true}}'),
            keys          = [resolveKey]),
        MarkTrait(name = pieceLayer, value = oddLayer),
        basic])
    oddsP.setTraits(*traits)
                                     
    # ----------------------------------------------------------------
    # Modify odds prototype
    btlP   = prototypes['BattleMarkers prototype']
    traits = btlP.getTraits()
    basic  = traits.pop()
    traits.append(MarkTrait(name = pieceLayer, value = btlLayer))
    btlP.setTraits(*traits,basic)
    
    # ----------------------------------------------------------------
    # Modify odds prototype
    resP   = prototypes['ResultMarkers prototype']
    traits = resP.getTraits()
    basic  = traits.pop()
    traits.append(MarkTrait(name = pieceLayer, value = resLayer))
    resP.setTraits(*traits,basic)
    
    # ----------------------------------------------------------------
    # Modify optionals prototype
    optionalP = prototypes['OptionalMarkers prototype']
    traits    = optionalP.getTraits()
    basic     = traits.pop()
    optionalP.setTraits(
        TriggerTrait(name       = '',
                     key        = toggleKey,
                     actionKeys = [setOptional],
                     property   = f'{{Phase=="Setup"}}'),
        ClickTrait(key = toggleKey,
                   context = False,
                   whole = True,
                   description = 'Toggle optional rule'),
        ReportTrait(setOptional,
                    report=(f'{{{debug}?("! "+BasicName+" toggle "+'
                            f'Name+" "+'
                            f'(Step_Level==1?"On":"Off")):""}}')),
        basic)

    # ----------------------------------------------------------------
    # Modify optionals prototype
    detachP = prototypes['DetachMarkers prototype']
    traits  = detachP.getTraits()
    basic   = traits.pop()
    detachP.setTraits(
        ClickTrait(key         = toggleKey,
                   context     = False,
                   whole       = True,
                   description = 'Toggle detachment'),
        ReportTrait(setOptional,
                    report=(f'{{{debug}?("! "+BasicName+" toggle "+'
                            f'LocationName+" "+Detached+": "+'
                            f'(Step_Level==1?"On":"Off")):""}}')),
        basic)

    # ----------------------------------------------------------------
    # Create die roller prototype
    traits = [
        CalculatedTrait(
            name = 'Die',
            expression = f'{{GetProperty("{diceName}_result")}}',
            description = 'Die roll'),
        GlobalHotkeyTrait(
            name         = '',
            key          = dieRoll,
            globalHotkey = diceKey,
            description  = 'Roll dice'),
        ReportTrait(diceKey,
                    report=f'{{{verbose}?("Rolled dice {diceName}"):""}}'),
        BasicTrait()
    ]
    prototypeContainer.addPrototype(name        = f'Dice prototype',
                                    description = f'Dice prototype',
                                    traits      = traits)
    # ----------------------------------------------------------------
    # Create dices roller prototype
    traits = [
        CalculatedTrait(
            name = 'AlliedDie',
            expression = f'{{GetProperty("AlliedDice_result")}}',
            description = 'Die roll'),
        CalculatedTrait(
            name = 'GermanDie',
            expression = f'{{GetProperty("GermanDice_result")}}',
            description = 'Die roll'),
        GlobalHotkeyTrait(
            name         = '',
            key          = dicesRoll,
            globalHotkey = dicesKey,
            description  = 'Roll dices'),
        ReportTrait(diceKey,
                    report=f'{{{debug}?("Rolled dices {dicesName}"):""}}'),
        BasicTrait()
    ]
    prototypeContainer.addPrototype(name        = f'Dices prototype',
                                    description = f'Dices prototype',
                                    traits      = traits)

    # ----------------------------------------------------------------
    # Create air power prototype
    traits = [
        PrototypeTrait(name='Dices prototype'),
        CalculatedTrait(
            name = 'AlliedAir',
            expression = f'{{Math.max(AlliedDie+AlliedAirWeather,0)}}',
            description = 'Available allied air units'),
        CalculatedTrait(
            name = 'GermanAir',
            expression = f'{{Math.max(GermanDie+GermanAirWeather,0)}}',
            description = 'Available german air units'),
        CalculatedTrait(
            name = 'Supremacy',
            expression = ('{(GermanAir>AlliedAir?"German":'
                          '(AlliedAir>GermanAir?"Allied":"No"))}'),
            description = 'Determine air supremacy'),
        GlobalPropertyTrait(
            ['',resolveAir+'Global',GlobalPropertyTrait.DIRECT,'{Supremacy}'],
            name = globalSupremacy),
        TriggerTrait(name       = '',
                     key        = resolveAir,
                     actionKeys = [dicesRoll,resolveAir+'Global']),
        ReportTrait(dicesRoll,
                    report=(f'{{{verbose}?("`"+'
                            f'"Allied air factors="+AlliedAirWeather+"+"+'
                            f'AlliedDie+"="+AlliedAir):""}}')),
        ReportTrait(dicesRoll,
                    report=(f'{{{verbose}?("`"+'
                            f'"German air factors="+GermanAirWeather+"+"+'
                            f'GermanDie+"="+GermanAir):""}}')),
        ReportTrait(dicesRoll,
                    report=(f'{{{debug}?("`"+Supremacy+" faction has "+'
                            f'"air supremacy"):""}}')),
        BasicTrait()
    ]
    prototypeContainer.addPrototype(name        = f'Air units prototype',
                                    description = f'Air units prototype',
                                    traits      = traits)
    
    # ----------------------------------------------------------------
    # Create weather prototype
    gloomy = ['Gloomy']
    sunny  = ['Sunny']
    snowy  = ['Snowy']
    wet    = ['Wet']
    stormy = ['Stormy']
    weatherTable = [gloomy*10,                             #16
                    sunny*2+gloomy*3+wet*4+snowy,          #17
                    sunny*2+gloomy*3+wet*4+snowy,          #18
                    sunny*2+gloomy*3+wet*4+snowy,          #19
                    sunny*2+gloomy*3+wet*4+snowy,          #20
                    sunny*2+gloomy*3+wet*4+snowy,          #21
                    sunny*2+gloomy*3+wet*4+snowy,          #22
                    sunny*6+gloomy*2+wet*2,                #23
                    sunny*6+gloomy*2+wet*2,                #24
                    sunny*6+gloomy*2+wet*2,                #25
                    sunny*6+gloomy*2+wet*2,                #26
                    sunny*6+gloomy*2+wet*2,                #27
                    sunny*2+gloomy*2+wet+snowy*3+stormy*2, #28
                    sunny*2+gloomy*2+wet+snowy*3+stormy*2, #29
                    sunny*2+gloomy*2+wet+snowy*3+stormy*2, #30
                    sunny*2+gloomy*2+wet+snowy*3+stormy*2] #31
    weatherAllied = [[-9]*8+[-8]*2,                        #16
                     [-2,-3,-9,-9,-9,-5,-4,-3,-3,-5],      #17
                     [-2,-3,-9,-9,-9,-5,-4,-3,-3,-5],      #18
                     [+1, 0,-9,-9,-8,-4,-3,-3,-2,-4],      #19
                     [+1, 0,-9,-9,-8,-4,-3,-3,-2,-4],      #20
                     [+1, 0,-9,-9,-8,-4,-3,-3,-2,-4],      #21
                     [+1, 0,-9,-9,-8,-4,-3,-3,-2,-4],      #22
                     [+2,+2,+2,+2,+1,+1,-9,-8,-4,-3],      #23
                     [+2,+2,+2,+2,+1,+1,-9,-8,-4,-3],      #24
                     [+2,+2,+2,+2,+1,+1,-9,-8,-4,-3],      #25
                     [+2,+2,+2,+2,+1,+1,-9,-8,-4,-3],      #26
                     [+2,+2,+2,+2,+1,+1,-9,-8,-4,-3],      #27
                     [+2,+1,-9,-8,-3,-4,-5,-6,-9,-9],      #28
                     [+2,+1,-9,-8,-3,-4,-5,-6,-9,-9],      #29
                     [+2,+1,-9,-8,-3,-4,-5,-6,-9,-9],      #30
                     [+2,+1,-9,-8,-3,-4,-5,-6,-9,-9]]      #31
    weatherGerman = [[-9]*8+[-8]*2,                        #16
                     [-6,-6,-9,-9,-9,-7,-7,-6,-6,-6],      #17
                     [-6,-6,-9,-9,-9,-7,-7,-6,-6,-6],      #18
                     [-6,-6,-9,-9,-8,-7,-6,-6,-6,-6],      #19
                     [-6,-6,-9,-9,-8,-7,-6,-6,-6,-6],      #20
                     [-6,-6,-9,-9,-8,-7,-6,-6,-6,-6],      #21
                     [-6,-6,-9,-9,-8,-7,-6,-6,-6,-6],      #22
                     [-6,-6,-6,-6,-7,-7,-9,-8,-7,-6],      #23
                     [-6,-6,-6,-6,-7,-7,-9,-8,-7,-6],      #24
                     [-6,-6,-6,-6,-7,-7,-9,-8,-7,-6],      #25
                     [-6,-6,-6,-6,-7,-7,-9,-8,-7,-6],      #26
                     [-6,-6,-6,-6,-7,-7,-9,-8,-7,-6],      #27
                     [-6,-7,-9,-8,-8,-6,-7,-7,-9,-9],      #28
                     [-6,-7,-9,-8,-8,-6,-7,-7,-9,-9],      #29
                     [-6,-7,-9,-8,-8,-6,-7,-7,-9,-9],      #30
                     [-6,-7,-9,-8,-8,-6,-7,-7,-9,-9]]      #31
    weatherTab = ('{{'+':'.join([f'EffTurn=={t+1}?('
                                 + ':'.join([f'Die=={d+1}?"{w}"'
                                             for d,w in enumerate(turn)])
                                 + ':"Wet")'
                                 for t, turn in enumerate(weatherTable)])
                  +':"Wet"}}')
    alliedTab =  ('{{'+':'.join([f'EffTurn=={t+1}?('
                                 + ':'.join([f'Die=={d+1}?{w}'
                                             for d,w in enumerate(turn)])
                                 + ':-9)'
                                 for t, turn in enumerate(weatherAllied)])
                  +':-9}}')
    germanTab =  ('{{'+':'.join([f'EffTurn=={t+1}?('
                                 + ':'.join([f'Die=={d+1}?{w}'
                                             for d,w in enumerate(turn)])
                                 + ':-9)'
                                 for t, turn in enumerate(weatherGerman)])
                  +':-9}}')
    moveWeather = key(NONE,0)+',moveWeather'
    traits = [
        PrototypeTrait(name='Dice prototype'),
        PrototypeTrait(name='Air units prototype'),
        CalculatedTrait(
            name = 'EffTurn',
            expression = f'{{Turn+({globalScenario}==22?6:0)}}',
            description = 'Resolve weather'),
        CalculatedTrait(
            name = 'Weather',
            expression = weatherTab,
            description = 'Resolve weather'),
        CalculatedTrait(
            name = 'AlliedAirWeather',
            expression = alliedTab,
            description = 'Resolve Allied air units weather'),
        CalculatedTrait(
            name = 'GermanAirWeather',
            expression = germanTab,
            description = 'Resolve German air units weather'),
        GlobalPropertyTrait(
            ['',setWeather,GlobalPropertyTrait.DIRECT,'{Weather}'],
            name        = globalWeather, # True on bad weather
            numeric     = False,
            description = 'Set global bad weather flag'),
        GlobalPropertyTrait(
            ['',setWeather+'Allied',GlobalPropertyTrait.DIRECT,
             '{AlliedAirWeather}'],
            name        = globalWeather+'Allied', # True on bad weather
            numeric     = True,
            description = 'Set global bad weather flag'),
        GlobalPropertyTrait(
            ['',setWeather+'German',GlobalPropertyTrait.DIRECT,
             '{GermanAirWeather}'],
            name        = globalWeather+'German', # True on bad weather
            numeric     = True,
            description = 'Set global bad weather flag'),
        TriggerTrait(name       = 'Resolve weather',
                     command    = '',
                     key        = rollWeather,
                     property   = f'{{{optWeather}==true}}',
                     actionKeys = [dieRoll,
                                   setWeather,
                                   setWeather+'Allied',
                                   setWeather+'German',
                                   moveWeather]),
        GlobalPropertyTrait(
            ['',fixOptional+'Weather',GlobalPropertyTrait.DIRECT,
             f'{{{optAir}?{optAir}:{optWeather}}}'],
            name = optWeather,
            numeric = True),
        GlobalPropertyTrait(
            ['',fixOptional+'River',GlobalPropertyTrait.DIRECT,
             f'{{{optOneFiveZero}?{optOneFiveZero}:{optRiver}}}'],
            name = optRiver,
            numeric = True),
        GlobalPropertyTrait(
            ['',fixOptional+'Supply',GlobalPropertyTrait.DIRECT,
             f'{{{optFuel}?{optFuel}:{optSupply}}}'],
            name = optSupply,
            numeric = True),
        GlobalHotkeyTrait(name         = '',
                          key          = start16Key+'Global',
                          globalHotkey = start16Key),
        GlobalHotkeyTrait(name         = '',
                          key          = start22Key+'Global',
                          globalHotkey = start22Key),
        GlobalHotkeyTrait(name         = '',
                          key          = fixOptional+'Global',
                          globalHotkey = cleanOptional),
        TriggerTrait(name       = 'Move pieces',
                     command    = '',
                     key        = setitupKey,
                     property   = f'{{{globalScenario}==16}}',
                     actionKeys = [start16Key+'Global']),
        TriggerTrait(name       = 'Move pieces',
                     command    = '',
                     key        = setitupKey,
                     property   = f'{{{globalScenario}==22}}',
                     actionKeys = [start22Key+'Global']),
        TriggerTrait(name       = 'Fix optional rules',
                     command    = '',
                     key        = fixOptional,
                     actionKeys = [fixOptional+'Weather',
                                   fixOptional+'River',
                                   fixOptional+'Supply',
                                   fixOptional+'Global']),
        ReportTrait(setitupKey,
                    report=(f'{{{debug}?("Setting up pieces for Dec "+'
                            f'{globalScenario}+" starting turn"):""}}')),
        ReportTrait(start16Key+'Global',
                    report=(f'{{{debug}?("Global Dec 16 "+'
                            f'{globalScenario}+" starting turn"):""}}')),
        ReportTrait(start22Key+'Global',
                    report=(f'{{{debug}?("Global Dec 22 "+'
                            f'{globalScenario}+" starting turn"):""}}')),
        ReportTrait(start22Key+'Incr',
                    report=(f'{{{debug}?("Global Dec 22 "+'
                            f'{globalScenario}+" set turn "+Turn):""}}')),
        ReportTrait(setWeather,
                    report=(f'{{{debug}?("Roll weather dice "+Die+" on turn "+'
                            f'EffTurn+" -> "+Weather+" -> "+'
                            f'{globalWeather}+" "+'
                            f'"German weather: "+GermanAirWeather+" "+'
                            f'"Allied weather: "+AlliedAirWeather+" "):""}}'))]
    # Make send-to traits
    send  = []
    trig  = []
    board = main['mapName']
    for w in ['Sunny','Gloomy','Snowy','Wet','Stormy']:
        send.append(
            SendtoTrait(mapName     = board,
                        boardName   = board,
                        name        = '',
                        restoreName = '',
                        restoreKey  = '',
                        zone        = 'Weather',
                        destination = 'R', #R for region
                        region      = f'{w.lower()}@Weather',
                        key         = moveWeather+w,
                        x           = 0,
                        y           = 0))
        trig.append(
            TriggerTrait(
                name       = f'Move weather to {w}',
                command    = '',
                key        = moveWeather,
                actionKeys = [moveWeather+w],
                property   = (f'{{Weather=="{w}"}}')))
        
    traits.extend(send+trig+[BasicTrait()])
    prototypeContainer.addPrototype(name        = f'Weather prototype',
                                    description = f'Weather prototype',
                                    traits      = traits)
    
    # ----------------------------------------------------------------
    # Create terrain prototype
    board = main['mapName']
    traits = [        
        CalculatedTrait(
            name        = 'AttackWoods',
            expression  = f'{{{globalWoods}.contains(":"+{defenderHex}+":")}}',
	    description = 'Check if defender is in Woods'),
        CalculatedTrait(
            name        = 'AttackFortified',
            expression  = (f'{{{globalFortified}.contains(":"+'
                           f'{defenderHex}+":")}}'),
	    description = 'Check if defender is in Fortified'),
        CalculatedTrait(
            name        = 'AttackTown',
            expression  = f'{{{globalTown}.contains(":"+{defenderHex}+":")}}',
	    description = 'Check if defender is in City'),
        CalculatedTrait(
            name        = 'AttackRough',
            expression  = (f'{{{globalRough}.contains(":"+'
                           f'{defenderHex}+":")}}'),
	    description = 'Check if defender is in Mountains'),
        CalculatedTrait(
            name        = 'OverRiver',
            expression  = (f'{{{globalRivers}.contains(":"+'
                           f'EffectiveHex+{defenderHex}+":")}}'),
	    description = 'Check if defender is in Rivers'),
        CalculatedTrait(
            name        = 'EffectiveHex',
            expression  = (f'{{LocationName}}'),
	    description = 'Effective hex occupied'),
        BasicTrait()
        ]
    prototypeContainer.addPrototype(name        = f'Terrain prototype',
                                    description = f'Terrain prototype',
                                    traits      = traits)

    # ----------------------------------------------------------------
    # Modify types prototype.  Set AF and DF bonuses
    todel   = ['motorised airborne',
               'motorised',
               'reconnaissance']
    # air prototype
    # fixed wing prototype
    # fortification prototype
    # fuel prototype
    # land prototype
    # installation prototype
    # 
    # brigade prototype
    # division prototype
    # platoon prototype
    # regiment prototype

    ground  = ['infantry',
               'infantry airborne',
               'infantry motorised',
               'infantry motorised airborne',
               'infantry armoured',
               'SF',
               'armoured',
               'armoured reconnaissance',
               'airborne motorised infantry']

    for gn in ground:
        proto  = prototypes.get(f'{gn} prototype',None)
        if not proto: continue
        traits = proto.getTraits()
        basic  = traits.pop()
        woods  = ('+(AttackWoods?-1:0)'
                  if 'motorised' in gn or 'armoured' in gn else '') 
        town   = ('+(AttackTown?-1:0)'
                  if 'motorised' in gn or 'armoured' in gn else '') 
        

        traits.extend([
            PrototypeTrait(name='Terrain prototype'),
            CalculatedTrait(
                name       = 'BonusDF',
                expression = (f'{{(AttackWoods?1:0)+'
                              f'(AttackRough?1:0)+'
                              f'(AttackFortified&&Faction=="German"?3:0)+'
                              f'({optSupply}&&Isolated_Level==2?-1:0)'
                              f'}}'),
                description = 'DF modifiers'),
            CalculatedTrait(
                name         = 'BonusAF',
                expression   = (f'{{(OverRiver?-1:0)'
                                f'{woods}{town}}}'),
                description  = 'AF modifiers')])
        traits.append(basic)
        proto.setTraits(*traits)

    air = ['fixed wing']
    for a in air:
        proto  = prototypes[f'{a} prototype']
        traits = proto.getTraits()
        basic  = traits.pop()
        traits.extend([
            CalculatedTrait(name         = 'BonusAF',
                            expression   = (f'{{0}}'),
                            description  = 'AF modifiers'),
            basic])
        proto.setTraits(*traits)

        
    # ----------------------------------------------------------------
    # Battle unit prototype update with forest, fortress, overriver rules
    battleUnitP = prototypes[battleUnit]
    traits      = battleUnitP.getTraits()
    basic       = traits.pop()
    effAF       = Trait.findTrait(traits,CalculatedTrait.ID,
                                  key='name',value='EffectiveAF')
    effDF       = Trait.findTrait(traits,CalculatedTrait.ID,
                                  key='name',value='EffectiveDF')
    oddsS       = Trait.findTrait(traits,CalculatedTrait.ID,
                                  key='name',value='OddsShift')
    calAF       = Trait.findTrait(traits,GlobalPropertyTrait.ID,
                                  key='name', value=battleAF)
    calDF       = Trait.findTrait(traits,GlobalPropertyTrait.ID,
                                  key='name', value=battleDF)
    calAFCmds   = calAF.getCommands()
    calAFCmd    = calAFCmds[0]
    calAFkey    = calAFCmd[1]
    calAFCmd[1] = calAFkey+'Real'
    calDFCmds   = calDF.getCommands()
    calDFCmd    = calDFCmds[0]
    calDFkey    = calDFCmd[1]
    calDFCmd[1] = calDFkey+'Real'
    calAF.setCommands(calAFCmds)
    calDF.setCommands(calDFCmds)
    
    if effAF: traits.remove(effAF)
    if effDF: traits.remove(effDF)
    # if oddsS: traits.remove(oddsS)

    toremove = []
    for trait in traits:
        if trait.ID != ReportTrait.ID: continue
        # print(trait['report'])
        if 'to total attack factor' in trait['report']:
            toremove.append(trait)
        if 'to total defence factor' in trait['report']:
            toremove.append(trait)
    for trait in toremove:
        # print('Remove',trait['report'])
        traits.remove(trait)
            
    printKey = key(NONE,0)+',print'
    traits.extend([
        CalculatedTrait(
            name        = 'EffectiveAF',
            expression  = f'{{(CF+BonusAF)}}',
            description = 'Calculate effective CF'),
        CalculatedTrait(
            name        = 'EffectiveDF',
            expression  = (f'{{(DF+BonusDF)}}'),
            description = 'Calculate effective CF'),
        GlobalPropertyTrait(
            ['',updateHex,GlobalPropertyTrait.DIRECT,
             f'{{(IsAttacker?{defenderHex}:LocationName)}}'],
            name        = defenderHex,
            numeric     = True,
            description = 'Update defender hex to this unit',
        ),
        TriggerTrait(
            name       = '',
            key        = calAFkey,
            actionKeys = [calAFCmd[1]]),
        TriggerTrait(
            name       = '',
            key        = calDFkey,
            actionKeys = [calDFCmd[1]]),
        TriggerTrait(
            name       = 'Print',
            command    = 'Print',
            key        = printKey,
            property   = f'{{{debug}}}',
            actionKeys = []),
        ReportTrait(printKey,
                    report=(f'{{BasicName+":"'
                            f'+" Hex="+EffectiveHex'
                            f'+" Edge="+EffectiveHex+{defenderHex}'
                            f'+" AF="+EffectiveAF'
                            f'+" DF="+EffectiveDF'
                            f'+" Faction="+Faction'
                            f'+" Attacker="+IsAttacker'
                            f'+" OverRiver="+OverRiver'
                            f'+" AttackRough="+AttackRough'
                            f'+" AttackWoods="+AttackWoods'
                            f'+" AttackTown="+AttackTown'
                            f'+" AttackFortified="+AttackFortified'
                            f'+" Current="+Current'
                            f'+" Step="+Step_Level'
                            f'+" Loss="+Loss_Level'
                            f'+" Isolated="+Isolated_Level'
                            f'}}')),
        ReportTrait(calAFCmd[1],
                    report = (f'{{{debug}?("! "+BasicName+'
                              f'" Hex="+EffectiveHex+" ("+LocationName+")"+'
                              f'" AF="+CF+"+"+BonusAF+" ("'
                              f'+(AttackRough?"Rough ":" ")'
                              f'+(AttackWoods?"Woods ":" ")'
                              f'+(AttackTown?"Town ":" ")'
                              f'+(AttackFortified?"Fortified ":" ")'
                              f'+(OverRiver?"River ":" ")'
                              f'+")="+EffectiveAF+"=>"'
                              f'+{battleAF}'
                              f'):""}}')),
        ReportTrait(calDFCmd[1],
                    report = (f'{{{debug}?("! "+BasicName+'
                              f'" Hex="+EffectiveHex+" ("+LocationName+")"+'
                              f'" Defended="+{defenderHex}+'
                              f'" DF="+DF+"+"+BonusDF+" ("'
                              f'+(AttackWoods?"Woods ":" ")'
                              f'+(AttackRough?"Rough ":" ")'
                              f'+(AttackTown?"Town ":" ")'
                              f'+(AttackFortified?"Fortified ":" ")'
                              f'+")="+EffectiveDF+"=>"'
                              f'+{battleDF}'
                              f'):""}}')),
        ReportTrait(updateHex,
                    report = (f'{{{debug}?("! "+BasicName+" in "+'
                              f'LocationName+" update hex -> "+'
                              f'{defenderHex}'
                              f'):""}}')),
        basic
        ])
    rest = RestrictCommandsTrait(keys          = [declareKey],
                                 name          = 'Disable when not in combat',
                                 hideOrDisable = RestrictCommandsTrait.DISABLE,
                                 expression    = f'{{{globalDeclare}==true}}')
    battleUnitP.setTraits(rest,*traits)
    #battleUnitP.setTraits(*traits)

    # ----------------------------------------------------------------
    # Battle calculation update with forest, fortress, overriver rules
    limitAF     = key(NONE,0)+',limitAF'
    limitDF     = key(NONE,0)+',limitDF'
    battleCalcP = prototypes[battleCalc]
    traits      = battleCalcP.getTraits()
    basic       = traits.pop()
    calcOdds    = Trait.findTrait(traits,TriggerTrait.ID,
                                  key   = 'key',         
                                  value = calcOddsKey)
    calcFrac    = Trait.findTrait(traits,TriggerTrait.ID,
                                  key   = 'key',         
                                  value = calcFracKey)
    batFrac    = Trait.findTrait(traits,CalculatedTrait.ID,
                                 key   = 'name',         
                                 value = 'BattleFraction')
    oldFrac    = batFrac['expression'][1:-1]
    
    traits.extend([
        # --- Set not combat phase ----------------------------------
        GlobalPropertyTrait(
            ['',checkDeclare,GlobalPropertyTrait.DIRECT,
             f'{{Phase!="German combat"&&'
             f'Phase!="German movement"&&'
             f'Phase!="Allied combat"&&'
             f'Phase!="Allied movement"}}'],
            name = globalDeclare,
            numeric = True,
            description = 'Check for (any) declare phase'),
        # --- Set not combat phase ----------------------------------
        GlobalPropertyTrait(
            ['',checkResolve,GlobalPropertyTrait.DIRECT,
             f'{{!Phase.contains("combat")}}'],
            name        = globalResolve,
            numeric     = True,
            description = 'Check for (any) resolve phase'),
        # --- Set not combat phase ----------------------------------
        GlobalPropertyTrait(
            ['',checkSetup,GlobalPropertyTrait.DIRECT,
             f'{{Phase!="Setup"||Turn!=1}}'],
            name        = globalSetup,
            numeric     = True,
            description = 'Check for setup phase'),
        # --- Update hex of defenders --------------------------------
        GlobalHotkeyTrait(
            name         = '',
            key          = optKey,
            globalHotkey = optKey,
            description  = 'Show optional rules'),
        # --- Update hex of defenders --------------------------------
        GlobalHotkeyTrait(
            name         = '',
            key          = updateHex,
            globalHotkey = updateHex,
            description  = 'Ask GKC to update defender hex'),
        # --- Limit total AF -----------------------------------------
        GlobalPropertyTrait(
            ['',limitAF,GlobalPropertyTrait.DIRECT,
             f'{{{battleAF}>10?10:({battleAF}<0?0:{battleAF})}}'],
            name        = battleAF,
            numeric     = True,
            description = 'Limit AF to from 0 to 10'),
        # --- Limit total DF -----------------------------------------
        GlobalPropertyTrait(
            ['',limitDF,GlobalPropertyTrait.DIRECT,
             f'{{{battleDF}>10?10:({battleDF}<0?0:{battleDF})}}'],
            name        = battleDF,
            numeric     = True,
            description = 'Limit DF to from 0 to 10'),
        # --- Reports ------------------------------------------------
        ReportTrait(checkDeclare,
                    report = (f'{{{debug}?("~ Check for declare phase "+'
                              f'Phase+" -> {globalDeclare}="+'
                              f'{globalDeclare}):""}}')),
        ReportTrait(checkResolve,
                    report = (f'{{{debug}?("~ Check for resolve phase "+'
                              f'Phase+" -> {globalResolve}-"+'
                              f'{globalResolve}):""}}')),
        ReportTrait(checkSetup,
                    report = (f'{{{debug}?("~ Check for setup phase "+'
                              f'Phase+" -> {globalSetup}-"+'
                              f'{globalSetup}):""}}')),
        ReportTrait(updateHex,
                    report = (f'{{{debug}?("~ "+BasicName+" Update def hex "+'
                              f'{defenderHex}):""}}')),
        ReportTrait(limitAF,
                    report = (f'{{{debug}?("~ "+BasicName+" Limit AF "+'
                              f'{battleAF}):""}}')),
        ReportTrait(limitDF,
                    report = (f'{{{debug}?("~ "+BasicName+" Limit DF "+'
                              f'{battleDF}):""}}')),
        basic
        ])
    keys = calcFrac.getActionKeys()
    calcFrac.setActionKeys(keys[:-1]+[limitAF,limitDF]+keys[-1:])
    keys = calcOdds.getActionKeys()
    calcOdds.setActionKeys([updateHex]+keys)
    battleCalcP.setTraits(*traits)
    
    
    # ----------------------------------------------------------------
    # German Fuel prototype 
    incr   = []
    decr   = []
    iCode  = f'{{"de fuel "+((Value>=35)?Value:(Value+1))}}'
    dCode  = f'{{"de fuel "+((Value<=0)?Value:(Value-1))}}'
    vCode  = (f'{{LocationName=="de fuel"?Start:'
             f'Integer.parseInt(LocationName.replaceAll("[^0-9]",""))}}')
    glb    = ''
    traits = [
        CalculatedTrait(
            name        = 'Value',
            expression  = vCode,
            description ='Calculate current value'),
        GlobalPropertyTrait(
            ['',resetFuelKey,GlobalPropertyTrait.DIRECT,f'{{0}}'],
            name        = glb,
            numeric     = True,
            description = f'German fuel'),
        TriggerTrait(
            name        = 'Fixed increment',
            command     = '',
            key         = turnFuelKey,
            actionKeys  = [incrFuelKey],
            loop        = True,
            count       = 5),
        ReportTrait(
            incrFuelKey,decrFuelKey,
            report = (f'{{{debug}?("~ "+BasicName+" "+Value):""}}')),
        ReportTrait(
            turnFuelKey,
            report = (f'{{{debug}?("! "+BasicName+" "+Value+'
                      f'" to be incremented"):""}}')),
        ReportTrait(
            turnFuelKey,
            report = (f'{{"` German fuel incremented automatically by 5"}}')),
        MarkTrait(name='Faction',value='German'),
        MarkTrait(name='Type',value='fuel'),
        BasicTrait()]
    prototypeContainer\
        .addPrototype(name        = f'German fuel prototype',
                      description = f'German fuel prototype',
                      traits      = traits)

    for oobn, oob in maps.items():
        if not oobn.startswith('German OOB'):
            continue
            
        oob.addMassKey(name         = 'Decrement Fuel',
                       buttonHotkey = key('-',ALT),
                       hotkey       = decrFuelKey,
                       buttonText   = '',
                       icon         = '/icons/32x32/go-previous.png',
                       reportSingle = True,
                       reportFormat = '',
                       target       = '',
                       filter       = f'{{BasicName == "de fuel"}}',
                       tooltip      = 'Decrement fuel counter')
        oob.addMassKey(name         = 'Increment fuel',
                       buttonHotkey = key('=',ALT_SHIFT),
                       hotkey       = incrFuelKey,
                       buttonText   = '',
                       icon         = '/icons/32x32/go-next.png',
                       reportSingle = True,
                       reportFormat = '',
                       target       = '',
                       filter       = f'{{BasicName == "de fuel"}}',
                       tooltip      = 'Increment fuel counter')
        

        
        
    # ----------------------------------------------------------------
    #
    # We create the traits once, since they will be copied to the prototypes
    # Phases to allow the isolate command
    enbIso   = {'Allied': [phaseNames[alliedSup],phaseNames[germanSup]],
                'German': [phaseNames[alliedSup],phaseNames[germanSup]] }
    more = [
        RestrictCommandsTrait(name          = 'Restrict Ctrl-Shift-I',
                              hideOrDisable = RestrictCommandsTrait.DISABLE,
                              expression    = '{!Phase.contains("Supply")}',
                              keys          = [insupplyKey]),
        TriggerTrait(
            name       = 'Real state change',
            command    = '',
            key        = stepOrElim,
            actionKeys = [curDecrKey],
            property   = (f'{{{current}>1}}')),
        TriggerTrait(
            name       = 'Real eliminate',
            command    = '',
            key        = stepOrElim,
            actionKeys = [eliminateKey],
            property   = (f'{{{current}==1}}')),
        TriggerTrait(
            name       = 'Step loss or eliminate',
            command    = 'Step loss',
            key        = stepKey,
            actionKeys = [stepOrElim]),
        LayerTrait(
            ['','isolated-mark.png'],
            ['','Out of supply +'],
            activateName = '',
            activateMask = '',
            activateChar = '',
            increaseName = 'Isolated',
            increaseMask = CTRL,
            increaseChar = 'I',
            decreaseName = '',
            decreaseMask = '',
            decreaseChar = '',
            resetKey     = resetIsolated,
            under        = False,
            underXoff    = 0,
            underYoff    = 0,
            name         = 'Isolated',
            loop         = False,
            description  = '(Un)Mark unit as isolated',
            always       = False,
            activateKey  = '',
            increaseKey  = isolatedKey,
            decreaseKey  = '',
            scale        = 1),
        LayerTrait(
            ['','step-loss-mark.png'],
            ['','Step loss +'],
            activateName = '',
            activateMask = '',
            activateChar = '',
            increaseName = 'Step loss',
            increaseMask = CTRL,
            increaseChar = 'L',
            decreaseName = '',
            decreaseMask = '',
            decreaseChar = '',
            resetKey     = '',#resetIsolated,
            under        = False,
            underXoff    = 0,
            underYoff    = 0,
            name         = 'Loss',
            loop         = False,
            description  = 'Add step loss',
            always       = False,
            activateKey  = '',
            increaseKey  = '',#lossKey,
            decreaseKey  = '',
            scale        = 1,
            follow       = True,
            expression   = f'{{Math.min(({current}%2)+1,Steps)}}'
        ),
        TriggerTrait(
            name       = 'Remove isolation mark',
            command    = 'In supply',
            key        = insupplyKey,
            actionKeys = [resetIsolated],
            property   = (f'{{Isolated_Level>=2}}')),
        ReportTrait(
            eliminateKey,
            report=(f'{{{debug}?("~ "+BasicName+" eliminated"):""}}')),
        ReportTrait(
            toggleStep,
            report=(f'{{{debug}?("~ "+BasicName+" step loss"):""}}')),
        ReportTrait(
            restoreKey,
            report=(f'{{{debug}?("~ "+BasicName+" restored"):""}}')),
        ReportTrait(
            isolatedKey,
            report=(f'{{{debug}?("~ "+BasicName'
                    f'+" isolated "+Isolated_Level):""}}')),
    ]
    restrict = \
        RestrictCommandsTrait(
            name           ='Restrict isolated command',
            hideOrDisable  = RestrictCommandsTrait.DISABLE,
            expression     = (f'{{!Phase.contains("Supply")||'
                              f'(!{optSupply}&&Faction=="Allied")'
                              f'}}'),
            keys           = [isolatedKey])
    # ----------------------------------------------------------------
    # Create a prototype to send a unit to army HQ holding box
    detachKey = ''
    for faction in ['German', 'Allied']:
        factionp = prototypes[f'{faction} prototype']
        ftraits  = factionp.getTraits()
        fbasic   = ftraits.pop()

        # Remove traits we don't want nor need
        fdel     = Trait.findTrait(ftraits,DeleteTrait.ID)
        fdel['key'] = deleteKey
        fdel['name'] = ''
        # ftraits.remove(fdel)

        ftraits.append(
            MarkTrait(
                name       = pieceLayer,
                value      = unitLayer))
        
        sftraits = None
        if faction == 'German':
            sftraits = ftraits.copy()
            sftraits.append(BasicTrait())
        
        # TBD - add german activation
        if faction == 'German':
            ftraits.extend([
                RestrictCommandsTrait(
                    name           ='Restrict activate command',
                    hideOrDisable  = RestrictCommandsTrait.DISABLE,
                    expression     = (f'{{Phase!="{phaseNames[germanAct]}"}}'),
                    keys           = [activateKey]),
                LayerTrait(
                    name         = 'Activation',
                    images       = ['','activate-mark.png'],
                    newNames     = ',+ activated',
                    increaseKey  = activateKey,
                    increaseName = 'Activate',
                    decreaseKey  = '',
                    decreaseName = '',
                    resetKey     = deactivateKey,
                    resetName    = 'Deactivate',
                    loop         = False),
                ReportTrait(
                    activateKey,
                    report = (f'{{{verbose}?("`"+BasicName+" now activated, "+'
                              f'"remember to use fuel"):""}}')),
                ReportTrait(
                    deactivateKey,
                    report = (f'{{{debug}?("~"+BasicName+" now deactivated"):'
                              f'""}}'))])
                           
        ftraits.append(restrict)
        ftraits.extend(more)
        ftraits.append(fbasic)
        
        factionp.setTraits(*ftraits)

        if not sftraits: continue

        btlp = Trait.findTrait(sftraits,PrototypeTrait.ID,
                               'name','wgBattleUnit')
        if btlp:
            #print('Remove battle unit prototype from faction prototype')
            sftraits.remove(btlp)
        
        prototypeContainer\
            .addPrototype(name        = f'German SF prototype',
                          description = f'German SF prototype',
                          traits      = sftraits)


    # ----------------------------------------------------------------
    # Get all pieces and add Eliminate action that moves the unit to
    # the OOB. This is also where we add the traits to move the game
    # turn marker on the turn track.
    #
    # Allied historical setup 
    alliedHist = {}
    pieces  = game.getPieces(asdict=False)
    germans = ['de ','ss ']
    allies  = ['al ','br ','us ']
    extra   = {
        True: {
            'us 9 ad': {'flipped': True },
            'us 28 mid': {'flipped': True },
            'al prepared 2': {'flipped': True },
            'al prepared 3': {'flipped': True },
            'al prepared 4': {'flipped': True },
            'al prepared 5': {'flipped': True },
            'al prepared 6': {'flipped': True },
            'al prepared 7': {'flipped': True },
            'ss 1 sfp':      {'start':   'Y5'},
            'ss 2 sfp':      {'start':   'Y5'}
        },
        False: {
            'us 78 mid':	{'start': 'W2'},
            'us 102 arr':	{'start': 'V2'},
            'al prepared 1':	{'start': 'V2'},
            'us 9 mid':		{'start': 'V3'},
            'us 99 mid':	{'start': 'V4'},
            'al prepared 2':	{'start': 'V4',	'flipped': True},
            'us e 30 mir':	{'start': 'R6'},
            'us 2 mid':		{'start': 'V5'},
            'al prepared 3':	{'start': 'V5', 'flipped': True},
            'br k 29 11 mir':	{'start': 'B4'},
            'al depot 3 1':	{'start': 'B4'},
            'us d 30 mir':	{'start': 'Q5'},
            'al depot 8 1':	{'start': 'Q5'},
            'us 1 mid':		{'start': 'U6'},
            'al prepared 4':	{'start': 'U6', 'flipped': True},
            'us c 30 mir':	{'start': 'R5'},
            'al depot 4 1':	{'start': 'R5'},
            'us 30 mid':	{'start': 'S6',	'flipped': True, 'loss': True },
            'al prepared 5':	{'start': 'S6', 'flipped': True},
            'us 7 ad':		{'start': 'S8'},
            'us f 82 mir':	{'start': 'P6'},
            'us 3 ad':		{'start': 'L6', 'loss':    True},
            'us 82 abmid':	{'start': 'O7',	'flipped': True},
            'us 9 ad':		{'start': 'R7',	'flipped': True, 'loss': True },
            'us g 82 mir':	{'start': 'N8'},
            'us 84 mid':	{'start': 'I8'},
            'br 29 11 ab':	{'start': 'B8',	'flipped': True},
            'us 112 28 mir':	{'start': 'Q9'},
            'us 101 abmid':	{'start': 'O14'},
            'al prepared 6':	{'start': 'O14'},
            'us 10 ad':		{'start': 'R16', 'flipped': True},
            'us b 10 mir':	{'start': 'T16'},
            'al prepared 7':	{'start': 'T16', 'flipped': True},
            'us 1 a 9 mir':	{'start': 'V16'},
            'al prepared 8':	{'start': 'V16', 'flipped': True},
            'us a 10 mir':	{'start': 'W17'},
            'us 4 mid':		{'start': 'X17', 'loss':    True },
            'al prepared 9':	{'start': 'X17', 'flipped': True},
            'al depot 5 1':	{'start': 'M2'},
            'al depot 1 1':	{'start': 'M10'},
            'us 28 mid':        {'elim': True },
            'us 106 mid':       {'elim': True },
            'us 14 arr':        {'elim': True },
            'us r 9 mir':       {'elim': True },
            'us 2 a 9 mir':     {'elim': True },
            'us 109 28 mir':    {'elim': True },
            'al depot 1 2':     {'elim': True },
            #
            'de 272 id':	{'start': 'X1',	'flipped': True, 'loss': True },
            'de prepared 1':	{'start': 'X1',	'flipped': True},
            'de 326 id':	{'start': 'X2'},
            'de 3 aid':		{'start': 'W3'},
            'de 277 id':	{'start': 'W4',	'flipped': True, 'loss': True},
            'de j 277 ir':	{'start': 'W5'},
            'de prepared 2':	{'start': 'W5',	'flipped': True},
            'de 12 id':		{'start': 'W6'},
            'ss 12 ad':		{'start': 'V6',	'loss':    True },
            'de 150 ab':	{'start': 'T6',	'loss':    True },
            'ss peiper ab':	{'start': 'Q6',	'flipped': True},
            'de 3 abid':	{'start': 'U7',	'loss':    True },
            'de prepared 3':	{'start': 'U7',	'flipped': True},
            'ss 1 ad':		{'start': 'S7',	'loss':    True },
            'de fb ab':		{'start': 'T7'}, # I7, wrong
            'de 18 id':		{'start': 'U8',	'loss':    True },
            'de 62 id':		{'start': 'U9',	'loss':    True },
            'ss 9 ad':		{'start': 'S9'},
            'de 560 id':	{'start': 'P9'},
            'de 116 ad':	{'start': 'M10'},
            'ss 2 ad':		{'start': 'S11'},
            'de a lehr air':	{'start': 'K12'},
            'de 26 id':		{'start': 'P12'},
            'de 2 ad':		{'start': 'M13'},
            'de b lehr air':	{'start': 'M14'},
            'de lehr ad':	{'start': 'M15', 'flipped': True,'loss': True },
            'de k 5 ir':	{'start': 'N15'},
            'de prepared 5':	{'start': 'N15', 'flipped': True},
            'de l 5 ir':	{'start': 'P15'},
            'de prepared 6':	{'start': 'P15', 'flipped': True},
            'de m 5 ir':	{'start': 'Q15'},
            'de 5 abid':	{'start': 'O16', 'flipped': True},
            'de prepared 7':	{'start': 'O16', 'flipped': True},
            'de 79 id':		{'start': 'W13'},
            'de 352 id':	{'start': 'S15', 'loss':    True },#T16
            'de prepared 4':	{'start': 'S15', 'flipped': True},
            'de fg ab':		{'start': 'X15'},
            'de 276 id':	{'start': 'U16', 'loss':    False },#?
            'de prepared 8':	{'start': 'U16', 'flipped': True},
            'de 212 id':	{'start': 'W16', 'loss':    True },
            'de prepared 9':	{'start': 'W16'},
            'de heydte abir':   {'elim': True }
        }
    }
    
    ctrl    = None
    detunit = None
    for piece in pieces:
        name    = piece['entryName'].strip()
        faction = name[:3]
        Faction = 'Allied' if faction in allies else 'German'
        parent  = piece.getParent()
        oob     = None
        is16    = True
        if parent and parent.TAG == AtStart.TAG:
            pboard = parent.getParent()
            #print(f'{name:20s} at {parent["name"]:30s} in {pboard["mapName"]}')
            oob = pboard
            is16 = pboard['mapName'].endswith('Dec 16')
            
        # Remove all traits from move pieces
        if name == 'scenario 16':
            traits               = piece.getTraits()
            basic                = traits.pop()
            step                 = Trait.findTrait(traits, LayerTrait.ID,
                                                   'name', 'Step')
            bky                  = key(NONE,0)+',scenario'
            step['resetKey']     = ''
            step['resetName']    = ''
            step['increaseKey']  = ''
            step['increaseName'] = ''
            step['follow']       = True
            step['expression']   = f'{{{globalScenario}==16?1:2}}'
            traits = [
                ClickTrait(
                    key         = '',
                    context     = True,
                    whole       = True,
                    description = 'Select start-up'),
                RestrictCommandsTrait(
                    name          = 'Restrict scenario choice',
                    hideOrDisable = RestrictCommandsTrait.DISABLE,
                    expression    = f'{{Phase!="Setup"}}',
                    keys          = [bky+'16',bky+'22']),
                GlobalPropertyTrait(
                    ['Dec 16',bky+'16',GlobalPropertyTrait.DIRECT, f'{{16}}'],
                    ['Dec 22',bky+'22',GlobalPropertyTrait.DIRECT, f'{{22}}'],
                    name        = globalScenario,
                    numeric     = True,
                    wrap        = True,
                    description = 'Select scenario'),
                RestrictAccessTrait(sides=[],
                                    description='Cannot move'),
                ReportTrait(bky+'16',bky+'22',
                            report=(f'{{"Set-up turn: "+{globalScenario}}}')),
                step,
                basic]
            piece.setTraits(*traits)
            
        elif name in ['de fuel']:
            traits               = piece.getTraits()
            basic                = traits.pop()
            traits               = [
                PrototypeTrait(name=f'German fuel prototype')]
            if oob is not None:
                traits.extend([
                    MarkTrait(name='Start',value=20 if is16 else 10),
                    MarkTrait(name="UnitScenario",
                              value=16 if is16 else 22),
                    SendtoTrait(
                        mapName     = oob['mapName'],
                        boardName   = oob['mapName'],
                        name        = '',
                        restoreName = '',
                        restoreKey  = '',
                        destination = 'R', #R for region
                        region      = iCode,
                        key         = incrFuelKey),
                    SendtoTrait(
                        mapName     = oob['mapName'],
                        boardName   = oob['mapName'],
                        name        = '',
                        restoreName = '',
                        restoreKey  = '',
                        destination = 'R', #R for region
                        region      = dCode,
                        key         = decrFuelKey)])

            
            traits.append(DeleteTrait(name='', key=deleteKey))
            traits.append(basic)
            piece.setTraits(*traits)
        elif 'bridge' in name:
            traits               = piece.getTraits()
            basic                = traits.pop()
            ftrait               = Trait.findTrait(traits,PrototypeTrait.ID,
                                                   'name',
                                                   f'{Faction} prototype')
            if ftrait is not None: traits.remove(ftrait)
            traits.append(MarkTrait(name = pieceLayer, value = bridgeLayer))
            traits.append(basic)
            piece.setTraits(*traits)
            
        elif 'depot' in name:
            traits               = piece.getTraits()
            basic                = traits.pop()
            ftrait               = Trait.findTrait(traits,PrototypeTrait.ID,
                                                   'name',
                                                   f'{Faction} prototype')
            step                 = Trait.findTrait(traits, LayerTrait.ID,
                                                   'name', 'Step')
            if ftrait is not None: traits.remove(ftrait)
            if step is not None: traits.remove(step)
            
            fullAF               = Trait.findTrait(traits, MarkTrait.ID,
                                                   'name', 'CF')
            fuel = 0            
            if fullAF is not None:
                fullAF['name']       = 'Fuel'
                fuel                 = int(fullAF['value'])
            for cf in ['DF']:
                trait = Trait.findTrait(traits, MarkTrait.ID,
                                        'name', cf)
                if trait is not None:
                    traits.remove(trait)

            ext  = extra[is16].get(name,{})
            el   = ext.get('elim',False)
            hx   = None
            ur   = Trait.findTrait(traits, MarkTrait.ID,
                                     'name', 'upper right')
            if ur is not None:
                st = ur['value']
                st = sub('[^=]+=','',st).strip()
                hx = st
            if oob is not None:
                ky      = start16Key if is16 else start22Key
                startup = None
                if el:
                    startup    = SendtoTrait(name        = '',
                                             key         = ky,
                                             mapName     = 'DeadMap',
                                             boardName   = f'{Faction} pool',
                                             destination = 'L',
                                             restoreName = '',
                                             restoreKey  = '',
                                             description = 'Elimate on start')
                
                elif hx is not None and hx != '':
                    # print(f'Start hex of {name} is set to {hx}')
                    dest    = 'G'
                    pos     = hx 
                    reg     = hx
                    startup = SendtoTrait(name        = '',
                                          key         = ky,
                                          mapName     = 'Board',
                                          boardName   = 'Board',
                                          destination = dest,
                                          region      = reg,
                                          restoreName = '',
                                          restoreKey  = '',
                                          position    = pos)
                if startup:
                    traits.append(startup)

                # Allied destroys own fuel depot 
                ret     = parent['location']
                destroy = SendtoTrait(name        = 'Destroy',
                                      key         = key('D'),
                                      mapName     = oob['mapName'],
                                      boardName   = oob['mapName'],
                                      destination = 'R',
                                      region      = ret,
                                      restoreName = '',
                                      restoreKey  = '',
                                      position    = ret)
                traits.append(destroy)
                traits.append(MarkTrait(name="UnitScenario",
                                        value=16 if is16 else 22))

            realKy  = key(NONE,0)+',capture'
            # Also increment German Fuel
            trig    = TriggerTrait(name       = 'Capture depot',
                                   command    = 'Capture',
                                   key        = eliminateKey,
                                   actionKeys = [realKy])
            elim    = SendtoTrait(name        = '',
                                  key         = realKy,
                                  mapName     = 'DeadMap',
                                  boardName   = f'{Faction} pool',
                                  destination = 'L',
                                  restoreName = '',
                                  restoreKey  = '',
                                  description = 'Real capture of depot')
            rept    = ReportTrait(
                realKy,
                report = f'{{"Captured fuel depot worth {fuel}, remember to adjust German fuel reserves"}}')
            traits.extend([elim,trig,rept])

            traits.append(DeleteTrait(name='', key=deleteKey))
            traits.append(MarkTrait(name = pieceLayer, value = unitLayer))
            traits.append(basic)
            piece.setTraits(*traits)
                    
        elif 'prepared' in name:
            traits               = piece.getTraits()
            basic                = traits.pop()
            ftrait               = Trait.findTrait(traits,PrototypeTrait.ID,
                                                   'name',
                                                   f'{Faction} prototype')
            step                 = Trait.findTrait(traits, LayerTrait.ID,
                                                   'name', 'Step')
            reducedAF            = Trait.findTrait(traits, MarkTrait.ID,
                                                   'name', 'ReducedCF')
            fullAF               = Trait.findTrait(traits, MarkTrait.ID,
                                                   'name', 'FullCF')            
            reducedAF['value']   = 0
            fullAF['value']      = 0
            # print(traits)
            # print(name,Faction,ftrait)
            if ftrait:           traits.remove(ftrait)
            ext                  = extra[is16].get(name,{})
            fl                   = ext.get('flipped',False)
            hx                   = ext.get('start',None)
            step['level']        = 2 if fl else 1
            if hx is None:
                ur = Trait.findTrait(traits, MarkTrait.ID,
                                     'name', 'upper right')
                if ur is not None:
                    st = ur['value']
                    st = sub('[^=]+=','',st).strip()
                    hx = st

            if oob is not None and hx is not None and hx != '':
                # print(f'Start hex of {name} is set to {hx}')
                dest    = 'G'
                pos     = hx 
                reg     = hx
                ky      = start16Key if is16 else start22Key
                startup = SendtoTrait(name        = '',
                                      key         = ky,
                                      mapName     = 'Board',
                                      boardName   = 'Board',
                                      destination = dest,
                                      region      = reg,
                                      restoreName = '',
                                      restoreKey  = '',
                                      position    = pos)
                traits.append(startup)

            if oob is not None:
                ret     = parent['location']
                elim    = SendtoTrait(name        = 'Return',
                                      key         = eliminateKey,
                                      mapName     = oob['mapName'],
                                      boardName   = oob['mapName'],
                                      destination = 'R',
                                      region      = ret,
                                      restoreName = '',
                                      restoreKey  = '',
                                      position    = ret)
                traits.append(elim)
                traits.append(MarkTrait(name="UnitScenario",
                                        value=16 if is16 else 22))

            traits.append(MarkTrait(name='Faction',        value=Faction))
            traits.append(MarkTrait(name='BonusDF',        value=0))
            traits.append(MarkTrait(name='BonusCF',        value=0))
            traits.append(MarkTrait(name='AttackWoods',    value=False))
            traits.append(MarkTrait(name='AttackRough',    value=False))
            traits.append(MarkTrait(name='AttackFortified',value=False))
            traits.append(MarkTrait(name='AttackTown',     value=False))
            traits.append(PrototypeTrait(name='wgBattleUnit'))
            traits.append(MarkTrait(name = pieceLayer, value = unitLayer))
            traits.append(DeleteTrait(name='', key=deleteKey))
            traits.append(basic)
            piece.setTraits(*traits)

        elif name.endswith(' aw'):
            traits               = piece.getTraits()
            basic                = traits.pop()
            ftrait               = Trait.findTrait(traits,PrototypeTrait.ID,
                                                   'name',
                                                   f'{Faction} prototype')
            step                 = Trait.findTrait(traits, LayerTrait.ID,
                                                   'name', 'Step')
            if ftrait:           traits.remove(ftrait)
            if step:             traits.remove(step)

            if oob is not None:
                ret     = parent['location']
                elim    = SendtoTrait(name        = 'Return to base',
                                      key         = eliminateKey,
                                      mapName     = oob['mapName'],
                                      boardName   = oob['mapName'],
                                      destination = 'R',
                                      region      = ret,
                                      restoreName = '',
                                      restoreKey  = '',
                                      position    = ret)
                traits.append(elim)
                traits.append(MarkTrait(name="UnitScenario",
                                        value=16 if is16 else 22))

            zone     = f'{"AL" if Faction=="Allied" else "DE"} air sorties'
            zonel    = zone.lower()[:2]
            missions = [['cap', 'CAP',[]],
                        ['cas', 'CAS',['Gloomy','Snowy','Stormy']]]
            if Faction == 'Allied':
                missions.extend([['interdict', 'Interdict',[]],
                                 ['air drop',  'Supply drop',['Stormy']]])

            # Perhaps filter on weather?
            misTraits = []
            repTraits = []
            resTraits = []
            for mr,mn,wr in missions:
                reg = f'{zonel} {mr}@{zone}'
                ky = key(NONE,0)+f',{faction}{mr}'
                # print(f'Air mission {mn}: {reg} {ky}')
                misTraits.append(
                    SendtoTrait(name        = f'Send to {mn}',
                                key         = ky,
                                mapName     = board,
                                boardName   = board,
                                destination = 'R',
                                region      = reg,
                                restoreName = '',
                                restoreKey  = '',
                                position    = reg))
                repTraits.append(
                    ReportTrait(
                        ky,
                        report = f'{{{debug}?("{Faction} {mn} air mission"):""}}'))
                rexp = ''
                if len(wr) > 0:
                    rexp = '||'.join([f'{globalWeather}=="{w}"' for w in wr])
                elif mn == 'CAP':
                    rexp = f'{globalSupremacy}!="{Faction}"'
                if rexp == '':
                    continue
                
                resTraits.append(
                    RestrictCommandsTrait(
                        name          = 'Restrict {mn}',
                        hideOrDisable = RestrictCommandsTrait.DISABLE,
                        expression    = f'{{{rexp}}}',
                        keys          = [ky]))
                
                
            traits.extend(resTraits)
            traits.extend(misTraits)
            traits.extend(repTraits)
            traits.append(MarkTrait(name='Faction',value=Faction))
            traits.append(MarkTrait(name='BonusDF',        value=0))
            traits.append(MarkTrait(name='BonusAF',        value=0))
            traits.append(MarkTrait(name='AttackWoods',    value=False))
            traits.append(MarkTrait(name='AttackRough',    value=False))
            traits.append(MarkTrait(name='AttackFortified',value=False))
            traits.append(MarkTrait(name='AttackTown',     value=False))
            traits.append(MarkTrait(name='OverRiver',      value=False))
            traits.append(CalculatedTrait(name='EffectiveHex',
                                          expression='{LocationName}'))
            traits.append(CalculatedTrait(name='EffectiveAF',
                                          expression='{CF}'))
            traits.append(PrototypeTrait(name='wgBattleUnit'))
            traits.append(MarkTrait(name = pieceLayer, value = unitLayer))
            traits.append(DeleteTrait(name='', key=deleteKey))
            traits.append(basic)
            piece.setTraits(*traits)
                
            # German and Allied pieces
        elif faction in germans+allies:
            traits               = piece.getTraits()
            basic                = traits.pop()
            step                 = Trait.findTrait(traits, LayerTrait.ID,
                                                   'name', 'Step')
            reducedCF            = Trait.findTrait(traits, MarkTrait.ID,
                                                   'name', 'ReducedCF')
            steps                = (1 if reducedCF is None or 'heydte' in name
                                    else 4)
            curr                 = steps
            ext                  = extra[is16].get(name,{})
            ls                   = ext.get('loss',   False)
            fl                   = ext.get('flipped',False)
            el                   = ext.get('elim',   False)
            if fl: curr -= curr//2
            if ls: curr -= 1
            
            traits.append(MarkTrait(name='CanFlip',
                                    value=False if reducedCF is None else True))
            traits.append(MarkTrait(name='Steps',
                                    value=steps))
            traits.append(
                DynamicPropertyTrait(
                    ['',curDecrKey,DynamicPropertyTrait.INCREMENT,'{-1}'],
                    name        = f'{current}',
                    value       = curr, 
                    numeric     = True,
                    min         = 0,
                    max         = steps,
                    description = 'Current # of steps left'))
            if step is not None:
                traits.remove(step)
                step['resetKey']     = ''
                step['resetName']    = ''
                step['increaseKey']  = ''
                step['increaseName'] = ''
                step['follow']       = True
                step['expression']   = fr'{{(Steps\/2)-({current}+1)\/2+1}}'
            else:
                #print(f'--- Step layer trait not found in {name}')
                pass

            
            hx    = ext.get('start',None)
            tn    = -1
            for trait in traits:
                if trait.ID == MarkTrait.ID:
                    if hx is None and trait['name'] == 'upper right':
                        st = trait['value']
                        st = sub('[^=]+=','',st).strip()
                        hx = st
                        if hx == '':
                            hx = None
                    if trait['name'] == 'upper left':
                        st  = trait['value']
                        st2 = sub('[^=]+=','',st).strip()
                        try:
                            tn = int(st2)
                        except:
                            tn = -1
            if hx is not None and '-' not in hx:
                tn = -1

            if oob is not None:
                ky      = start16Key if is16 else start22Key
                startup = None
                if  el:
                    startup  = SendtoTrait(name        = '',
                                           key         = ky,
                                           mapName     = 'DeadMap',
                                           boardName   = f'{Faction} pool',
                                           destination = 'L',
                                           restoreName = '',
                                           restoreKey  = '',
                                           description = 'Elimate on start')
                elif tn <= 0 and hx is not None and hx != '':
                    #print(f'Start hex of {name} is set to {hx} from {oob["mapName"]}')
                    dest    = 'G'
                    pos     = hx 
                    reg     = hx
                    startup = SendtoTrait(name        = '',
                                          key         = ky,
                                          mapName     = 'Board',
                                          boardName   = 'Board',
                                          destination = dest,
                                          region      = reg,
                                          restoreName = '',
                                          restoreKey  = '',
                                          position    = pos)

                if startup:
                    traits.append(startup)
                traits.append(MarkTrait(name="UnitScenario",
                                        value=16 if is16 else 22))

            # Remove duplicate prototypes - e.g., "infantry" from
            # "infantry airborne"
            dupl = ['infantry motorised',
                    'infantry armoured',
                    'infantry airborne',
                    'infantry motorised airborne']
            for dup in dupl:
                if Trait.findTrait(traits,PrototypeTrait.ID,
                                   key='name',
                                   value=f'{dup} prototype') is None:
                    continue
                # print(dup)
                for o in dup.split(' '):
                    t = Trait.findTrait(traits,PrototypeTrait.ID,
                                        key='name',
                                        value=f'{o} prototype')
                    if t is not None:
                        # print(f'Removing {o} from {dup} piece')
                        traits.remove(t)
                        
            rest = RestrictCommandsTrait(
                'Restrict step command',
                hideOrDisable = 'Disable',
                expression    = (
                    f'{{!(Phase.contains("Supply")||'
                    f'Phase.contains("air drop"))}}'),
                keys       = [stepKey])
            # traits.insert(0,rest)

            # Add step trait back in
            if step is not None: traits.append(step)

            # Modify basic prototype for SF units
            if name.endswith(" sfp"):
                #print(f'Modifying the SF unit {name}')
                facp = Trait.findTrait(traits,PrototypeTrait.ID,
                                        'name', f'{Faction} prototype')
                if facp:
                    facp['name'] = f'{Faction} SF prototype'

            # Set traits 
            traits.append(basic)
            piece.setTraits(*traits)

        # Add prototype traits to game turn marker.  Note, we
        # remove the basic piece trait from the end of the list,
        # and add it in after adding the other traits: The basic
        # piece trait _must_ be last.
        elif name == 'game turn':
            gtraits  = piece.getTraits()
            markers  = Trait.findTrait(gtraits,
                                       PrototypeTrait.ID,
                                       key = 'name',
                                       value = 'Markers prototype')
            flip     = Trait.findTrait(gtraits,LayerTrait.ID)
            if markers is not None:
                gtraits.remove(markers)
            flip['increaseName'] = ''
            
            gbasic   = gtraits.pop()
            # Add the prototypes we created above 
            gtraits.extend([PrototypeTrait(pnn + ' prototype')
                            for pnn in turnp])
            # Do not allow movement
            gtraits.append(RestrictAccessTrait(sides=[],
                                               description='Cannot move'))
            gtraits.append(gbasic)
            piece.setTraits(*gtraits)

        elif name == 'weather':
            # Q7
            traits = piece.getTraits()
            basic  = traits.pop()

            # Add weather prototype 
            traits.append(PrototypeTrait('Weather prototype'))

            # Remove markers prototype 
            prot   = Trait.findTrait(traits,PrototypeTrait.ID,
                                     key   = 'name',
                                     value = 'Markers prototype')
            if prot is not None: traits.remove(prot)

            # Modify layer prototype to follow property 
            step   = Trait.findTrait(traits,LayerTrait.ID,
                                     key   = 'name',
                                     value = 'Step')
            traits.remove(step)

            # Add it back
            traits.append(basic)
            piece.setTraits(*traits)

                            
        elif name == 'initiative':
            # R6
            traits = piece.getTraits()
            basic  = traits.pop()
            step   = Trait.findTrait(traits, LayerTrait.ID,
                                     'name', 'Step')
            useKy = key(NONE,0)+',useInitiative'
            mvKy  = key(NONE,0)+',moveInitiative'
            step['newNames']     = 'Allied +,German +'
            step['increaseName'] = ''
            step['increaseKey']  = useKy;
            traits.remove(step)

            reg = ('{Step_Level==1?'
                   '"de initiative":"al initiative"}')
            traits.extend([
                SendtoTrait(name        = '',
                            key         = mvKy,
                            mapName     = 'Board',
                            boardName   = 'Board',
                            destination = 'R',
                            region      = reg,
                            restoreName = '',
                            restoreKey  = '',
                            position    = ''),
                step,
                TriggerTrait(name       = 'Use',
                             command    = 'Use',
                             key        = stepKey,
                             actionKeys = [mvKy,useKy])])
                
            if oob is not None:
                if 'German' in oob['mapName']:
                    step['level'] = 2
                    
            prot   = Trait.findTrait(traits,PrototypeTrait.ID,
                                     'name', 'Markers prototype')
            if prot: traits.remove(prot)
            prot   = Trait.findTrait(traits,PrototypeTrait.ID,
                                     'name', ' prototype')
            if prot: traits.remove(prot)

            if oob is not None and is16:
                traits.append(
                    SendtoTrait(name        = '',
                                key         = start16Key,
                                mapName     = 'Board',
                                boardName   = 'Board',
                                destination = 'R',
                                region      = 'de initiative',
                                restoreName = '',
                                restoreKey  = '',
                                position    = 'de initiative'))
            if oob is not None and not is16:
                traits.append(
                    SendtoTrait(name        = '',
                                key         = start22Key,
                                mapName     = 'Board',
                                boardName   = 'Board',
                                destination = 'R',
                                region      = 'al initiative',
                                restoreName = '',
                                restoreKey  = '',
                                position    = 'al initiative'))
            if oob is not None:
                traits.append(MarkTrait(name="UnitScenario",
                                        value=16 if is16 else 22))
                
            piece.setTraits(*traits,basic)
                

        elif name == hidden:
            traits = piece.getTraits()
            basic  = traits.pop()
            showN  = key(NONE,0)+',showNotes'
            initD  = key(NONE,0)+',initDice'
            traits = [
                TriggerTrait(name       = 'Show notes window for tutorial',
                             command    = '',
                             key        = tutorialKey,
                             property   = f'{{{isTutorial}==true}}',
                             actionKeys = [showN]),
                # TriggerTrait(name       = 'Initial die roll',
                #              command    = '',
                #              key        = dicesKey,
                #              actionKeys = [initD]),
                GlobalHotkeyTrait(name         = '',
                                  key          = showN,
                                  globalHotkey = key('N',ALT),
                                  description  = 'Show notes window'),
                GlobalPropertyTrait(
                    ['',toggleBias,GlobalPropertyTrait.DIRECT,
                     f'{{!{globalNormal}}}'],
                    name        = globalNormal,
                    numeric     = True,
                    wrap        = True,
                    description = 'Toggle use of normal dice'),
                GlobalPropertyTrait(
                    ['',toggleBias,GlobalPropertyTrait.DIRECT,
                     f'{{!{globalBiased}}}'],
                    name        = globalBiased,
                    numeric     = True,
                    wrap        = True,
                    description = 'Toggle use of biased dice'),
                ReportTrait(toggleBias,
                            report = (f'{{"Dice: normal="+{globalNormal}'
                                      f'+" biased="+{globalBiased}}}')),
                # GlobalPropertyTrait(
                #     ['',dicesKey,GlobalPropertyTrait.DIRECT,'{1}'],
                #     name        = 'AlliedDice_result',
                #     numeric     = True,
                #     min         = 1,
                #     max         = 10,
                #     wrap        = True,
                #     description = 'Initial set of dice'),
                # GlobalPropertyTrait(
                #     ['',dicesKey,GlobalPropertyTrait.DIRECT,'{1}'],
                #     name        = 'GermanDice_result',
                #     numeric     = True,
                #     min         = 1,
                #     max         = 10,
                #     wrap        = True,
                #     description = 'Initial set of dice'),
                # GlobalHotkeyTrait(name         = '',
                #                   key          = initD,
                #                   globalHotkey = dicesKey,
                #                   description  = 'Initial die roll'),
                # ReportTrait(showN,tutorialKey,
                #             report = f'{{"Show notes "+{isTutorial}}}'),
            ]+traits
            piece.setTraits(*traits,basic)
            
        #elif name == 'odds marker 11':
        #    traits = piece.getTraits()
        #    basic  = traits.pop()
        #    basic['filename'] = 'sb-icon.png'

        elif name.startswith('opt '):
            optName = name.replace('opt ','').strip()
            gpName  = name.replace(' ','')
            
            traits = piece.getTraits()
            basic  = traits.pop()
            step   = Trait.findTrait(traits,LayerTrait.ID,
                                     key = 'name',
                                     value = 'Step')
            step['newNames']     = ['+ enabled','+ disabled']
            step['increaseName'] = ''
            step['increaseKey']  = ''
            step['follow']       = True
            step['expression']   = f'{{{gpName}==true?1:2}}'
            traits.extend([
                MarkTrait(name='OptName',value=optName),
                GlobalPropertyTrait(
                    ["",setOptional,GlobalPropertyTrait.DIRECT,
                     f'{{!{gpName}}}'],
                    name = gpName,
                    numeric = True,
                    description = f'Toggle {gpName}'),
                basic
            ])
            piece.setTraits(*traits)
        elif name == 'control':
            ctrl = piece

        elif name == "detached":
            detunit = piece
            traits  = piece.getTraits()
            basic   = traits.pop()
                
            step   = Trait.findTrait(traits,LayerTrait.ID,  'name', 'Step')
            
            if step:
                step['increaseName'] = ''
                step['increaseKey']  = ''
                step['follow']       = True
                step['expression']   = '{Detached?1:2}'

                traits.append(
                    DynamicPropertyTrait(
                        ['',toggleKey,DynamicPropertyTrait.DIRECT,
                         '{!Detached}'],
                        name =   'Detached',
                        value   = False,
                        numeric = True))

            piece.setTraits(*traits,basic)
            

    # -----------------------------------------------------------------
    # Grid on detachement sheets
    cellSize = 71.5
    cellOff  = int((cellSize+.5)//2)
    vOff     = int(21)

    def setDetState(piece,on=False):
        traits = piece.getTraits()
        basic  = traits.pop()
        val    = Trait.findTrait(traits,DynamicPropertyTrait.ID)
        if val:
            val['value'] = on
            
        piece.setTraits(*traits,basic)

    preset = {'Allied':
              {
                  16: { (1,7):  {'on':True},
                        (2,7):  {'on':True},
                        (3,7):  {'on':True},
                        (4,17): {'on':True},
                        (5,17): {'on':True},
                       },
                  22: { ( 1, 7):  {'na':True},
                        ( 2, 7):  {'on':True},
                        ( 3, 7):  {'na':True},
                        ( 1,17):  {'na':True},
                        ( 2,17):  {'na':True},
                        ( 3,17):  {'na':True},
                        ( 4,17):  {'on':True},
                        ( 5,17):  {'na':True},
                        ( 6,17):  {'na':True},
                        ( 7,17):  {'na':True},
                        ( 8,17):  {'na':True},
                        ( 9,17):  {'na':True},
                        (10,17):  {'na':True},
                        (11,17):  {'na':True},
                        (12,17):  {'na':True},
                        ( 1,29):  {'na':True},
                        ( 2,29):  {'na':True},
                        ( 3,29):  {'na':True},
                        ( 4,29):  {'na':True},
                        ( 5,29):  {'na':True},
                        ( 6,29):  {'na':True},
                        ( 7,29):  {'na':True},
                        ( 8,29):  {'na':True},
                        ( 9,29):  {'na':True},
                        (10,29):  {'na':True},
                        (11,29):  {'na':True},
                        (12,29):  {'na':True},                        
                       }
              },
              'German':
              {
                  16: { },
                  22: { ( 1,14): {'on':True},
                        ( 2,14): {'on':True},
                        (11,18): {'on':True},
                        (12,18): {'on':True},
                        (13,18): {'on':True},
                        (10,31): {'on':True},
                       }
              }
            }
    for oobn, oob in maps.items():
        if 'Detachments' not in oobn:
            continue

        allied = 'Allied' in oobn
        is16   = 'Dec 16' in oobn
        
        ncol1  = 12 if allied else  9
        ncol2  =  3 if allied else  4
        nrow1  = 29 if allied else 16
        nrow2  =  7 if allied else 19
        
        #print(oobn)
        zoned = oob.getBoardPicker()[0].getBoards().get(oobn).getZonedGrids()[0]
        zones  = zoned.getZones()
        zone   = list(zones.values())[0]
        grid   = zone.getSquareGrids(True)[0]
        num    = grid.getNumbering()[0]
        #print(' Zones',grid,num)

        grid['dx']      = cellSize
        grid['dy']      = cellSize
        grid['x0']      = cellOff
        grid['y0']      = cellOff+vOff
        grid['visible'] = False
        num['vDescend'] = False
        num['vOff']     = 0
        num['sep']      = '@'
        num['visible']  = False

        for col in range(1,ncol1+1):
            for row in range(1,nrow1+1):
                s = preset.get('Allied' if allied else 'German',{})\
                          .get(16 if is16 else 22,{})\
                          .get((col,row),{})
                if s.get('na',False):
                    continue
                
                a = oob.addAtStart(name     = f'detachcheck{col:02d}{row:02d}',
                                   location = f'{col}@{row}')
                p = a.addPiece(detunit)
                setDetState(p, s.get('on',False))
                

        for col in range(ncol1+1,ncol1+ncol2+1):
            for row in range(nrow1+1,nrow1+nrow2+1):
                s = preset.get('Allied' if allied else 'German',{})\
                          .get(16 if is16 else 22,{})\
                          .get((col,row),{})
                if s.get('na',False):
                    continue
                
                a = oob.addAtStart(name     = f'detachcheck{col:02d}{row:02d}',
                                   location = f'{col}@{row}')
                p = a.addPiece(detunit)
                setDetState(p, s.get('on',False))
                
            
            
#
# EOF
#

                      
    
    
