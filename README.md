# Battle of the Bulge-Day - Smithsonian edition

[[_TOC_]]

This is my remake of the wargame _Battle of the Bulge - Smithsonian
edition_.  The original game was published by Avalon Hill Game Company
in 1991.

The _Battle of the Bulge - Smithsonian edition_ is a variant of the
classic AHGC Battle of the Bulge, geared toward new-comers to
hex'n'counter board wargaming.  The game was published, but has since
gone out of print, as part of the _American History_ series that AHGC
developed together with the Smithsonian Museum in United States of
America.  Other games in the series include

- [D-Day](https://gitlab.com/wargames_tex/sdday_tex)
  simulating Allied invasion of Western Europe in 1944.
- [Gettysburg](https://boardgamegeek.com/boardgame/1576/gettysburg-125th-anniversary-edition) -
  the civil war battle 
- [Guadalcanal](https://boardgamegeek.com/boardgame/1375/guadalcanal)
  which was the start of the US pacific offensive.  This simulates
  mainly the naval part of the battle.
- [Midway](https://boardgamegeek.com/image/568159/midway) - the aerial
  battle of the islands in the middle of the Pacific Ocean 
- [Mustangs](https://boardgamegeek.com/image/6051696/mustangs) -
  dogfights during WWII. 
- [We the people](https://boardgamegeek.com/boardgame/620/we-people)
  Not really a hex'n'counter game, but a novel approach using cards
  (instead of dice) and interconnected spaces on a board.

See also Print'n'Play versions of the sister games [_Gettysburg -
Smithsonian Edition_](https://gitlab.com/wargames_tex/sgb_tex) and
[_D-Day_- Smithsonian Edition_](https://gitlab.com/wargames_tex/sdday_tex).

## Game mechanics 

|              |                    |
|--------------|--------------------|
| Period       | WWII               |
| Level        | Operational        |
| Hex scale    | 3.5km (2.2 miles)  |
| Unit scale   | division (xx)      |
| Turn scale   | 1 day              |
| Unit density | High               |
| # of turns   | 16                 |
| Complexity   | 1 of 10            |
| Solitaire    | 8 of 10            |

Features:
- Campaign
- Abstracted air operations
- Fuel consumption
- Scenarios
- Optional rules

## About this rework 

This rework is entirely new.  All text and graphics is new and nothing
is copied verbatim from the original materials. 

I have restructured and rewritten the rules somewhat (the text is all
new), as well as added illustrations of the rules. 

Corrections from the
[errata](http://www.grognard.com/errata1/bulge91.txt) available online
has been incorporated. 

> _A note_: The errata has several problems it self.  The electronic
> version seems to be the result of an OCR scan which has failed in
> some places.
>
> Most notably are the corrections to the German 22nd of December
> setup.  The original placements of the German FB panzer brigade (DE
> FB AB), and German 352nd Volksgrenadier Division (DE 352 ID) were
> hexes I7 and T16, respectively, which are clearly wrong.
> 
> In the errata, these are said to be "7T" and "71W", respectively,
> which are _also_ clearly wrong (there's no such hexes).
>
> Based on a review of historical maps and descriptions, the starting
> hexes for DE FB AB and DE 352 ID in the 22nd of December setup, are
> set to T7 and S15, respectively.
>
> If someone has access to the _original_ errata, or know of some
> other reason why the new placements should be changed, then I would
> very much like to hear from you.  Thanks.

## The board 

The board is exactly too big (30.5cm x 35.5cm, or 12" x 14") to fit on
standard paper formats, so everything has been scaled down a bit
(94%). 

## The files 

The distribution consists of the following files 

- [BattleOfTheBulgeSmithsonianA4.pdf][] This is a single document that contains
  everything: The rules, charts, counters, and order of battle, and
  the board split into two parts.
  
  This document is meant to be printed on a duplex A4 printer.  The
  charts, OOB charts, counters, and board will be on separate sheets
  and can be separated from the rules.
  
- [BattleOfTheBulgeSmithsonianA4Booklet.pdf][] This document _only_ contains the
  rules.  It is meant to be printed on a duplex A4 printer with long
  edge binding.  This will produce 6 sheets which should be folded
  down the middle of the long edge and stabled to form an 24-page A5
  booklet of the rules.
  
- [boardA3.pdf][] holds the entire board on a sheet of A3 paper.
  Print this and glue on to a sturdy piece of cardboard (1.5mm poster
  carton seems a good choice). 
  
- [boardA4.pdf][] has the board split over two A4 sheets of paper.
  Cut out and glue on to sturdy cardboard.  Note that one sheet should
  be cut at the crop marks and glued over the other sheet.  Take care
  to align the sheets. 
  
- [materialsA4.pdf][] holds the charts, counters, and OOB.  It is
  meant to be printed on A4 paper.  Print and glue on to a relatively
  thick piece of cardboard (1.5mm or so) or the like.
  
  The counters are double-sided, which means one has to take care to
  align the sides.   My procedure is roughly as follows: 
  - I like to glue the full counter table on to a
	medium thick piece of cardboard (0.75mm poster carton) and let it
	dry between two heavy books. 
  - Then, once dried, I cut down the middle between the front and the
	back, _but not the whole way through_.
  - Then I fold over the back-side along the cut line and glue the
	back on to the front, and let it dry between two heavy books.  
  - Once dried I start to cut out the counters.  
    - I do that by cutting groves (i.e., not all the way through)
      along the vertical and horizontal lines.
	- Finally, I cut entirely through the cardboard along the longest
      direction, _except_ for a small bit in one end.  
    - Then, I can cut through in the other direction quite easily, and
      separate out the counters. 
  
If you only have access to US Letter (and possibly Tabloid) printer,
you can use the following files

| *A4 Series*                      | *Letter Series*                      |
| ---------------------------------|--------------------------------------|
| [BattleOfTheBulgeSmithsonianA4.pdf][]        | [BattleOfTheBulgeSmithsonianLetter.pdf][]  	  |
| [BattleOfTheBulgeSmithsonianA4Booklet.pdf][] | [BattleOfTheBulgeSmithsonianLetterBooklet.pdf][] |
| [materialsA4.pdf][]	           | [materialsLetter.pdf][]	          |
| [boardA4.pdf][]	               | [boardLetter.pdf][]	              |
| [boardA3.pdf][]	               | [boardTabloid.pdf][]	              |

Note, the US letter files are scaled down to 95% relative to the A4
files.  This is to fit everything in on the same scale.  This should
not be a problem for most users as most of the world is using A4
rather than the rather obscure Letter format. 

Download [artifacts.zip][] to get all files for both kinds of paper
formats. 

## VASSAL modules 

Also available is a [VASSAL](https://vassalengine.org) module

- [VMOD Scenario][BattleOfTheBulgeSmithsonian.vmod]

The module is generated from the same sources as the Print'n'Play
documents, and contains the rules as an embedded PDF.  The modules
features

- Battle markers 
- Automatic odds markers 
- Automatic battle resolution, including for Allied strategic bombing
  missions
- Optional rules selection
- Automatic move points replenish on move points phase
- Embedded rules 
- Moved and battle markers automatically cleared 
- Symbolic dice 
- Separate layers for units and markers

## Previews

![Board](.imgs/hexes.png)
![Charts](.imgs/tables.png)
![Counters](.imgs/chits.png)
![Front](.imgs/front.png)
![Allied OOB Dec 16, 1944](.imgs/al-oob16.png)
![Allied OOB Dec 22, 1944](.imgs/al-oob22.png)
![German OOB Dec 16, 1944](.imgs/de-oob16.png)
![German OOB Dec 22, 1944](.imgs/de-oob22.png)
![Probabilities of combat outcomes](.imgs/probabilities.png)
![VASSAL module](.imgs/vassal.png)
![VASSAL Detail](.imgs/vassal-detail.png)
![Components](.imgs/photo_components.png)
![Detail](.imgs/photo_detail.png)

## Articles 

- Martin, B., "The Beauty of the Bulge - A Player's Look at Bulge
  '91", 
  [_The General_, *Vol.27*, #5, p6-8](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2027%20No%205.pdf#page=6)
- Martin, B., "Crossing the Line - Considerations for the German
  Player of Bulge '91",   [_The General_, *Vol.27*, #5,
  p9-10](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2027%20No%205.pdf#page=9)
- Martin, B., "Holding the Line - Suggestions for the Allied Player of
  Bulge '91",    [_The General_, *Vol.27*, #5,
  p11-12](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2027%20No%205.pdf#page=11)
- Blumberg, A., "Wacht Am Rhein Reversed - The Reduction of the
  Ardennes Salient", [_The General_, *Vol.27*, #5,
  p13-17](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2027%20No%205.pdf#page=13)
- Heller, R. & Grant, J.B.Jr., "Ardennes Breakthrough II- Suggested
  opening moves for the German player", [_The General_, *Vol.29*, #5,
  p36-37](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2029%20No%205.pdf#page=36)
  
## Implementation 

The whole package is implemented in LaTeX using my package
[_wargame_](https://gitlab.com/wargames_tex/wargame_tex).  This package,
combined with the power of LaTeX, produces high-quality documents,
with vector graphics to ensure that everything scales.   

[artifacts.zip]: https://gitlab.com/wargames_tex/sbotb_tex/-/jobs/artifacts/master/download?job=dist

[BattleOfTheBulgeSmithsonianA4.pdf]: https://gitlab.com/wargames_tex/sbotb_tex/-/jobs/artifacts/master/file/BattleOfTheBulgeSmithsonian-A4-master/BattleOfTheBulgeSmithsonianA4.pdf?job=dist
[BattleOfTheBulgeSmithsonianA4Booklet.pdf]: https://gitlab.com/wargames_tex/sbotb_tex/-/jobs/artifacts/master/file/BattleOfTheBulgeSmithsonian-A4-master/BattleOfTheBulgeSmithsonianA4Booklet.pdf?job=dist
[materialsA4.pdf]: https://gitlab.com/wargames_tex/sbotb_tex/-/jobs/artifacts/master/file/BattleOfTheBulgeSmithsonian-A4-master/materialsA4.pdf?job=dist
[boardA3.pdf]: https://gitlab.com/wargames_tex/sbotb_tex/-/jobs/artifacts/master/file/BattleOfTheBulgeSmithsonian-A4-master/boardA3.pdf?job=dist
[boardA4.pdf]: https://gitlab.com/wargames_tex/sbotb_tex/-/jobs/artifacts/master/file/BattleOfTheBulgeSmithsonian-A4-master/boardA4.pdf?job=dist
[BattleOfTheBulgeSmithsonian.vmod]: https://gitlab.com/wargames_tex/sbotb_tex/-/jobs/artifacts/master/file/BattleOfTheBulgeSmithsonian-A4-master/BattleOfTheBulgeSmithsonian.vmod?job=dist


[BattleOfTheBulgeSmithsonianLetter.pdf]: https://gitlab.com/wargames_tex/sbotb_tex/-/jobs/artifacts/master/file/BattleOfTheBulgeSmithsonian-Letter-master/BattleOfTheBulgeSmithsonianLetter.pdf?job=dist
[BattleOfTheBulgeSmithsonianLetterBooklet.pdf]: https://gitlab.com/wargames_tex/sbotb_tex/-/jobs/artifacts/master/file/BattleOfTheBulgeSmithsonian-Letter-master/BattleOfTheBulgeSmithsonianLetterBooklet.pdf?job=dist
[materialsLetter.pdf]: https://gitlab.com/wargames_tex/sbotb_tex/-/jobs/artifacts/master/file/BattleOfTheBulgeSmithsonian-Letter-master/materialsLetter.pdf?job=dist
[boardTabloid.pdf]: https://gitlab.com/wargames_tex/sbotb_tex/-/jobs/artifacts/master/file/BattleOfTheBulgeSmithsonian-Letter-master/boardTabloid.pdf?job=dist
[boardLetter.pdf]: https://gitlab.com/wargames_tex/sbotb_tex/-/jobs/artifacts/master/file/BattleOfTheBulgeSmithsonian-Letter-master/boardLetter.pdf?job=dist


